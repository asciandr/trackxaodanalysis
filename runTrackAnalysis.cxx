#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include "TrackxAODAnalysis/TrackxAODAnalysis/TrackxAODAnalysis.h"
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>

void runTrackAnalysis (const std::string& submitDir)
{

  xAOD::Init().ignore();

  SH::SampleHandler sh;

  //Input file(s)

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-slhc/users/PixelRadDamage/betaVersionBenchmarks/ZmumuSiHits_withLA/");
  //  SH::ScanDir().filePattern("mc16_13TeV.147407.PowhegPythia8_AZNLO_Zmumu.IDTIDE_benchmark6_file*_sihit.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/b/battagm/");
  //  SH::ScanDir().filePattern("DAOD_IDTRKVALID.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-ibl/DAOD/");
  //  SH::ScanDir().filePattern("mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s3170.DAOD_IDTRKVALID_TestMDN.pool.root").scan(sh,inputFilePath);
  //  SH::ScanDir().filePattern("mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.simul.HITS.e3668_s3170.DAOD_IDTRKVALID_TestMDN.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-ibl/DAOD/");
  //  SH::ScanDir().filePattern("data15_13TeV.00279169.physics_Main.recon.*.DAOD_IDTRKVALID.f628_m1453_r7350.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/tmp/battagm/data15_13TeV.00279169.physics_Main.merge.DAOD_IDTIDE.r9264_p3083_p3083_tid11253396_00");
  //  SH::ScanDir().filePattern("DAOD_IDTIDE.11253396._0000*.pool.root.1").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/tmp/battagm/group.perf-idtracking.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.e7142_e5984_s3126_r11212_IdealNomV5_EXT0");
  //  SH::ScanDir().filePattern("group.perf-idtracking.21461298.EXT0._0*.AOD.root").scan(sh,inputFilePath);

  //   const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/e/eballabe/public/runHV/test-100events/run2018-400V/");
  //   SH::ScanDir().filePattern("mc2018rad_13TeV.363664.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-ibl/DAOD/Cosmics2015/");
  //  SH::ScanDir().filePattern("mc14_cos.CosSimPixVolSolOffTorOff.*.IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-ibl/DAOD/Cosmics2020/");
  //  SH::ScanDir().filePattern("data20_calib.00384960.physics_IDCosmic.merge.RAW._lb*.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-ibl/data/PaperRun2/DAOD_IDTRKVALID/mc2018_rad.RDO.sihit.v2/");
  // SH::ScanDir().filePattern("mc2018rad_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.*.IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/tmp/battagm/group.perf-idtracking.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.e5362_e5984_s3126_r11212_lwtnn_NoWeight_V03_EXT0/");
  //  SH::ScanDir().filePattern("group.perf-idtracking.22770128.EXT0._0*.AOD.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/tmp/battagm/group.perf-idtracking.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.e6337_e5984_s3126_r11212_lwtnn_NoWeight_V03_EXT0");
  //  SH::ScanDir().filePattern("group.perf-idtracking.22772196.EXT0._0*.AOD.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/tmp/battagm/group.perf-idtracking.data15_13TeV.00279169.physics_Main.Rel21.NewModel_V1_EXT1/");
  //  SH::ScanDir().filePattern("group.perf-idtracking.23056782.EXT1._0*.DAOD_IDTRKVALID.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/a/asciandr/TrackingCP/LorentzAngle/fluence_6.4e14_thetaL_0.12");
  //  SH::ScanDir().filePattern("user.asciandr.361107.PP8_Zmumu.simul.DAOD_IDTRKVALID.HV400_fl6.4_ThL0.12_10kEv.e3601_s3126.pool.root").scan(sh,inputFilePath);
  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/b/battagl/atlas/");
  SH::ScanDir().filePattern("test.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);
  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/det-ibl/DAOD");
  //  SH::ScanDir().filePattern("data15_13TeV.00279169.physics_Main.recon.ESD_ZMUMU._0*.DAOD_IDTRKVALID.f628_m1453_r7350.root").scan(sh,inputFilePath);
  //  SH::ScanDir().filePattern("data18_13TeV.00349309.physics_Main.daq.RAW._lb*.data.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);
  //  SH::ScanDir().filePattern("data18_13TeV.00364076.physics_Main.daq.RAW._lb*.MDN.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);
  //  SH::ScanDir().filePattern("mc2018rad_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.022021.*.*V.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);
  
  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/a/asciandr/TrackingCP/lwtnn_LorentzAngle/user.asciandr.364707.PP8_jetjet_JZ7WithSW.simul.DAOD_IDTRKVALID.lwtnn_HV400_fl6.4_ThL0.12_40kEv.e7142_e5984_s3126_EXT0/");
  //  SH::ScanDir().filePattern("user.asciandr.23011620.EXT0._00*.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/unpledged/group-tokyo/datafiles/PIXEL/VALIDATAION/11.27.2020/IDTRKVALID/mc2018rad_sihit/");
  //SH::ScanDir().filePattern("mc2018_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.00*.XAOD.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/perf-idtracking/CTIDEOfficial/validation/newDB/");
  //  SH::ScanDir().filePattern("output_ttbar.ltwnnNumPos.NewDBv1.AOD.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("");
  //  SH::ScanDir().filePattern("newIBLcalib_IDTRKVALID.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/atlascerngroupdisk/perf-idtracking/CTIDEOfficial/validation");
  //  SH::ScanDir().filePattern("output_ttbar.ltwnnNumPos.AOD.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/a/asciandr/TrackingCP/LorentzAngle/");
  //  SH::ScanDir().filePattern("mc2018rad_ThetaL0.157_RadDamSensorSimPlanarTool.voltage300_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.05.400V_1kEvents.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/a/asciandr/TrackingCP/LorentzAngle/user.asciandr.361107.PP8_Zmumu.simul.DAOD_IDTRKVALID.HV400_fl6.4_ThL0.12_10kEv.e3601_s3126_EXT0/");

  //SH::ScanDir().filePattern("user.asciandr.22892834.EXT0._0000*.DAOD_IDTRKVALID.pool.root").scan(sh,inputFilePath);

  //  const char* inputFilePath = gSystem->ExpandPathName ("/tmp/battagm/ScanV0/");
  //  SH::ScanDir().filePattern("group.perf-idtracking.JZ7W_lwtnnRel21_ScanV0*_EXT0.AOD.root").scan(sh,inputFilePath);


  sh.setMetaString ("nc_tree", "CollectionTree");
  sh.print ();

  EL::Job job;  job.sampleHandler (sh);
  job.options()->setDouble (EL::Job::optMaxEvents, -1); 

  TrackxAODAnalysis *alg = new TrackxAODAnalysis;

  alg->SetName ("TrackxAODAnalysisAlg");
  job.algsAdd (alg);

  EL::DirectDriver driver;

  driver.submit (job, submitDir);
}
