Setup:
```
mkdir TrackxAODAnalysis
cd TrackxAODAnalysis
setupATLAS
lsetup "asetup AnalysisBase,here,22.2.79"
```

Clone and build package:
```
git clone ssh://git@gitlab.cern.ch:7999/asciandr/trackxaodanalysis.git source
mkdir build run
cd build
cmake ../source
make -j8
source x86*/setup.sh
```

Run:
```
cd ../run
ln -s ../source/runTrackxAODAnalysis.py .
./runTrackxAODAnalysis.py --submission-dir test
```
