#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/OutputStream.h>

#include <AsgMessaging/MessageCheck.h>
#include "AsgTools/AnaToolHandle.h"
#include <TrackxAODAnalysis/TrackxAODAnalysis.h>

//
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetConstituentVector.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackStateValidationAuxContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

using xAOD::IParticle;

#include<TFile.h>
#include<TVector2.h>
#include<TVector3.h>

// this is needed to distribute the algorithm to the workers
ClassImp(TrackxAODAnalysis)

namespace TrackState {
  enum MeasurementType {
    unidentified = 0,
    Pixel      = 1,
    SCT        = 2,
    TRT        = 3,
    MDT        = 4,
    CSC        = 5,
    RPC        = 6,
    TGC        = 7,
    Pseudo     = 8,
    Vertex     = 9,
    Segment    = 10,
    SpacePoint = 11,
    LArCal     = 12,
    TileCal    = 13,
    STGC       = 14,
    MM         = 15,
    NumberOfMeasurementTypes=16
  };
}

namespace TrackStateOnSurface {
  enum TrackStateOnSurfaceType {
    /** This is a measurement, and will at least contain a Trk::MeasurementBase*/
    Measurement=0,
 
    /** This represents inert material, and so will contain MaterialEffectsBase */
    InertMaterial=1, 
            
    /** This represents a brem point on the track,
     * and so will contain TrackParameters and MaterialEffectsBase */
    BremPoint=2, 
         
    /** This represents a scattering point on the track,
     * and so will contain TrackParameters and MaterialEffectsBase */
    Scatterer=3, 
          
    /** This represents a perigee, and so will contain a Perigee
     * object only*/
    Perigee=4, 
         
    /** This TSoS contains an outlier, that is, it contains a
     * MeasurementBase/RIO_OnTrack which was not used in the track
     * fit*/
    Outlier=5,
          
    /** A hole on the track - this is defined in the following way.
     * A hole is a missing measurement BETWEEN the first and last
     * actual measurements. i.e. if your track starts in the SCT,
     * you should not consider a missing b-layer hit as a hole.*/
    Hole=6,
    /** For some reason this does not fall into any of the other categories
     * PLEASE DO NOT USE THIS - DEPRECATED!*/
    Unknown=7,
           
    /** This TSOS contains a CaloEnergy object*/
    CaloDeposit=8,
              
    /**
     * This TSOS contains a Trk::MeasurementBase
     */
    Parameter=9,
            
    /**
     * This TSOS contains a Trk::FitQualityOnSurface
     */
    FitQuality=10,
    NumberOfTrackStateOnSurfaceTypes=11
  };
}

TrackxAODAnalysis :: TrackxAODAnalysis ()
{

}

EL::StatusCode TrackxAODAnalysis :: setupJob (EL::Job& job)
{
 
  // configure aux info in data set

  isMC = true;

  isCosmic = false;
  hasSiHits = true; //true;
  hasPixelNN = false; //false
  hasPixelVector = false; //true;
  hasPixelToT = true; //false;
  hasRefit = false; //true;

  // configure output

  doGenTree = false;
  doClusterTree = false;
  doTrackTree = true;

  doDensity = false;

  storeHoles = true;
  storeOutliers = false;
  doSplitTracks = false;
  storePixelNN = false;

  //jet

  doJets = false;
  JetCollectionName = "AntiKt4EMPFlowJets";   
  //JetCollectionName = "AntiKt4EMTopoJets";
  minJetPt = 100.; //50.

  //track selection

  minTrackPt = 0.7;
  minPixelHits = 2;
  minSiHits = 6;
  minTRTHits = 0;
  maxD0 = 100.0;
  maxDeltaZ = 10000.0;

  selectPixelOverlaps = false;
  selectIBLHits = true;
  select3DHits  = false;

  // ------------------------------------------------------

  if(isCosmic){
    minTrackPt = 1.0;
    doSplitTracks = true;
    hasPixelToT = true;
    doClusterTree = false;
    selectPixelOverlaps = false;
    minTRTHits = 0;
    minPixelHits = 1;
    minSiHits = 4;
    maxD0 = 100.0;
    maxDeltaZ = 5000.0;
  }
  if(!isMC){
    hasSiHits = false;
  }

  job.useXAOD ();

  /*  
  EL::OutputStream outForAODTree("aodtree");
  job.outputAdd ( outForAODTree );
  */

  EL::OutputStream outForSplitTrkTree("splittrktree");
  job.outputAdd ( outForSplitTrkTree );
  
  EL::OutputStream outForTrkTree("trktree");
  job.outputAdd ( outForTrkTree );
  
  EL::OutputStream outForClusterTree("clustertree");
  job.outputAdd ( outForClusterTree );
  
  EL::OutputStream outForGenTree("gentree");
  job.outputAdd ( outForGenTree );
 
  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init( "TrackxAODAnalysis" ).ignore(); // call before opening first file
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TrackxAODAnalysis :: histInitialize ()
{

  TFile *outputGenFile = wk()->getOutputFile("gentree");
  m_gentree = new TTree("gentree","gentree");
  m_gentree->SetDirectory(outputGenFile);
  m_gentree->Branch("runNumber",&m_runNumber);
  m_gentree->Branch("eventNumber",&m_eventNumber);
  m_gentree->Branch("lumiBlock",&lumiB);
  m_gentree->Branch("bcid",&bcid);
  m_gentree->Branch("bosonPt",&bosonPt);
  m_gentree->Branch("truthCharge",&truthCharge);
  m_gentree->Branch("truthPdgId",&truthPdgId);
  m_gentree->Branch("truthPt",&truthPt);
  m_gentree->Branch("truthPhi",&truthPhi);
  m_gentree->Branch("truthEta",&truthEta);
  m_gentree->Branch("truthTheta",&truthTheta);
  m_gentree->Branch("truthD0",&trueD0);
  m_gentree->Branch("truthZ0",&trueZ0);
  m_gentree->Branch("truthMindR",&mindR);
  m_gentree->Branch("truthPVtxX",&truthPVtxX);
  m_gentree->Branch("truthPVtxY",&truthPVtxY);
  m_gentree->Branch("truthPVtxZ",&truthPVtxZ);
  m_gentree->Branch("genVtxR",&genVtxR);
  m_gentree->Branch("genVtxZ",&genVtxZ);
  m_gentree->Branch("parentFlavour",&parentFlav);
  m_gentree->Branch("trackCharge",&trackCharge);
  m_gentree->Branch("trackPt",&trackPt);
  m_gentree->Branch("trackPhi",&trackPhi);
  m_gentree->Branch("trackEta",&trackEta);
  m_gentree->Branch("trackTheta",&trackTheta);
  m_gentree->Branch("trackNPixelLayers",&nPixelLayers);
  m_gentree->Branch("trackNPixelHits",&nPixelHits);
  m_gentree->Branch("trackNPixelHoles",&nPixelHoles);
  m_gentree->Branch("trackNPixelOutliers",&nPixelOutliers);
  m_gentree->Branch("trackNSCTHits",&nSCTHits);
  m_gentree->Branch("trackNSCTHoles",&nSCTHoles);
  m_gentree->Branch("trackNTRTHits",&nTRTHits);
  m_gentree->Branch("trackNIBLHits",&nIBLHits);
  m_gentree->Branch("trackNBLHits",&nBLHits);
  
  m_gentree->Branch("trackNIBLSharedHits",&nIBLSharedHits);
  m_gentree->Branch("trackNIBLSplitHits",&nIBLSplitHits);
  m_gentree->Branch("trackNBLSharedHits",&nBLSharedHits);
  m_gentree->Branch("trackNBLSplitHits",&nBLSplitHits);
  m_gentree->Branch("trackNIBLExpectedHits",&nIBLExpectedHits);
  m_gentree->Branch("trackNBLExpectedHits",&nBLExpectedHits);
  m_gentree->Branch("trackNSCTSharedHits",&nSCTSharedHits);
  
  m_gentree->Branch("trackD0",&trackD0);
  m_gentree->Branch("trackZ0",&trackZ0);
  m_gentree->Branch("trackChi2",&trackChi2);
  m_gentree->Branch("trackNdof",&trackNdof);
  m_gentree->Branch("trackTruthMatchProb",&trackMatchProb);
  m_gentree->Branch("pVtxX",&pVtxX);
  m_gentree->Branch("pVtxY",&pVtxY);
  m_gentree->Branch("pVtxZ",&pVtxZ);


  TFile *outputHitFile = wk()->getOutputFile("clustertree");
  m_clustertree = new TTree("clustertree","clustertree");
  m_clustertree->SetDirectory(outputHitFile);
  m_clustertree->Branch("runNumber",&m_runNumber);
  m_clustertree->Branch("eventNumber",&m_eventNumber);
  m_clustertree->Branch("timeStamp",&m_timeStamp);
  m_clustertree->Branch("lumiBlock",&lumiB);
  m_clustertree->Branch("bcid",&bcid);
  m_clustertree->Branch("averagePU",&m_averagePU);
  m_clustertree->Branch("eventPU",&m_eventPU);
  m_clustertree->Branch("mcFlag",&mcFlag);
  m_clustertree->Branch("nbOfTracks",&nbOfTracks);
  m_clustertree->Branch("nbOfFwdTracks",&nbOfFwdTracks);
  m_clustertree->Branch("clusterLayer",&clusterLayer);
  m_clustertree->Branch("clusterIsEndCap",&clusterIsEndCap);
  m_clustertree->Branch("clusterToT",&clusterToT);
  m_clustertree->Branch("clusterCharge",&clusterCharge);
  m_clustertree->Branch("clusterLVL1A",&clusterLVL1A);
  m_clustertree->Branch("clusterNPixelsX",&clusterNPixelsX);
  m_clustertree->Branch("clusterNPixelsY",&clusterNPixelsY);
  m_clustertree->Branch("clusterPhiModule",&clusterPhiModule);
  m_clustertree->Branch("clusterEtaModule",&clusterEtaModule);
  m_clustertree->Branch("clusterLocalX",&clustLocalX);
  m_clustertree->Branch("clusterLocalY",&clustLocalY);
  m_clustertree->Branch("clusterX",&clusterGlobalX);
  m_clustertree->Branch("clusterY",&clusterGlobalY);
  m_clustertree->Branch("clusterZ",&clusterGlobalZ);
  m_clustertree->Branch("clusterSiHitBarcode",&clusterSiHitBarcode);
  m_clustertree->Branch("clusterTruthBarcode",&clusterTruthBarcode);
  m_clustertree->Branch("clusterTruthPtcPt",&clusterTruthPtcPt);
  m_clustertree->Branch("clusterTruthPtcPdgId",&clusterTruthPtcPdgId);
  m_clustertree->Branch("clusterTrackPt",&clusterTrackPt);
 
  m_clustertree->Branch("clusterIsSplit",&clusterIsSplit);
  m_clustertree->Branch("clusterSplitProb1",&clusterSplitProb1);
  m_clustertree->Branch("clusterSplitProb2",&clusterSplitProb2);
 
  m_clustertree->Branch("pixelIndex",&clus_pixelIndex);
  m_clustertree->Branch("pixelRow",&clus_pixelRow);
  m_clustertree->Branch("pixelCol",&clus_pixelCol);
  m_clustertree->Branch("pixelCharge",&clus_pixelCharge);
  m_clustertree->Branch("pixelToT",&clus_pixelToT);


  TFile *outputTrkFile = wk()->getOutputFile("trktree");
  m_trktree = new TTree("trktree","trktree");
  m_trktree->SetDirectory(outputTrkFile);
  m_trktree->Branch("runNumber",&m_runNumber);
  m_trktree->Branch("eventNumber",&m_eventNumber);
  m_trktree->Branch("timeStamp",&m_timeStamp);
  m_trktree->Branch("lumiBlock",&lumiB);
  m_trktree->Branch("bcid",&bcid);
  m_trktree->Branch("averagePU",&m_averagePU);
  m_trktree->Branch("eventPU",&m_eventPU);
  m_trktree->Branch("mcFlag",&mcFlag);

  m_trktree->Branch("HLT_j100",&m_HLT_j100);
  m_trktree->Branch("HLT_j150",&m_HLT_j150);
  m_trktree->Branch("HLT_j200",&m_HLT_j200);
  m_trktree->Branch("HLT_j300",&m_HLT_j300);
  m_trktree->Branch("HLT_j320",&m_HLT_j320);
  m_trktree->Branch("HLT_j360",&m_HLT_j360);
  m_trktree->Branch("HLT_j380",&m_HLT_j380);
  m_trktree->Branch("HLT_j400",&m_HLT_j400);
  m_trktree->Branch("HLT_j420",&m_HLT_j420);
  m_trktree->Branch("HLT_j460",&m_HLT_j460);
  m_trktree->Branch("HLT_MBTS",&m_HLT_MBTS);

  m_trktree->Branch("iblP2Cut",&iblP2Cut); 
  m_trktree->Branch("iblP3Cut",&iblP3Cut); 

  m_trktree->Branch("nbOfPVtxs",&nPVtx);

  m_trktree->Branch("pVtxX",&pVtxX);
  m_trktree->Branch("pVtxY",&pVtxY);
  m_trktree->Branch("pVtxZ",&pVtxZ);

  m_trktree->Branch("pVtxXErr",&pVtxXErr);
  m_trktree->Branch("pVtxYErr",&pVtxYErr);
  m_trktree->Branch("pVtxZErr",&pVtxZErr);
  m_trktree->Branch("pVtxNTrk",&pVtxNTrk);

  m_trktree->Branch("truthPVtxX",&truthPVtxX);
  m_trktree->Branch("truthPVtxY",&truthPVtxY);
  m_trktree->Branch("truthPVtxZ",&truthPVtxZ);

  m_trktree->Branch("trackNPixelHits",&nPixelHits);
  m_trktree->Branch("trackNPixelHoles",&nPixelHoles);
  m_trktree->Branch("trackNPixelLayers",&nPixelLayers);
  m_trktree->Branch("trackNPixelOutliers",&nPixelOutliers);
  m_trktree->Branch("trackNIBLHits",&nIBLHits);
  m_trktree->Branch("trackNSCTHits",&nSCTHits);
  m_trktree->Branch("trackNSCTHoles",&nSCTHoles);
  m_trktree->Branch("trackNTRTHits",&nTRTHits);
  m_trktree->Branch("trackNBLHits",&nBLHits);
  m_trktree->Branch("trackNSplitIBLHits",&nIBLSplitHits);
  m_trktree->Branch("trackNSplitBLHits",&nBLSplitHits);
  m_trktree->Branch("trackNSharedIBLHits",&nIBLSharedHits);
  m_trktree->Branch("trackNSharedBLHits",&nBLSharedHits);
  m_trktree->Branch("trackNL2Hits",&nL2Hits);
  m_trktree->Branch("trackNL3Hits",&nL3Hits);
  m_trktree->Branch("trackNECHits",&nECHits);
  m_trktree->Branch("trackMinDistIBLHit",&minDistToIBLHit);
  m_trktree->Branch("trackMinDistL2Hit",&minDistToL2Hit);

  m_trktree->Branch("trackNIBLExpectedHits",&nIBLExpectedHits);
  m_trktree->Branch("trackNBLExpectedHits",&nBLExpectedHits);
  m_trktree->Branch("trackNSCTSharedHits",&nSCTSharedHits);

  m_trktree->Branch("nbOfTracks",&nbOfTracks);
  m_trktree->Branch("trackCharge",&trackCharge);
  m_trktree->Branch("trackPt",&trackPt);
  m_trktree->Branch("trackPhi",&trackPhi);
  m_trktree->Branch("trackEta",&trackEta);
  m_trktree->Branch("trackTheta",&trackTheta);
  m_trktree->Branch("trackqOverP",&trackqOverP);
  m_trktree->Branch("trackD0",&trackD0);
  m_trktree->Branch("trackZ0",&trackZ0);
  m_trktree->Branch("trackD0PV",&trackD0PV);
  m_trktree->Branch("trackZ0PV",&trackZ0PV);
  m_trktree->Branch("trackD0Err",&trackD0Err);
  m_trktree->Branch("trackZ0Err",&trackZ0Err);
  m_trktree->Branch("trackqOverPErr",&trackqOverPErr);
  m_trktree->Branch("trackDeltaZSinTheta",&trackDeltaZSinTheta);
  m_trktree->Branch("trackRefX",&trackRefX);
  m_trktree->Branch("trackRefY",&trackRefY);
  m_trktree->Branch("trackRefZ",&trackRefZ);

  if(doJets){
    m_trktree->Branch("trackJetdR",&trackJetdR);
    m_trktree->Branch("trackJetPt",&trackJetPt);
    m_trktree->Branch("trackJetFlavour",&trackJetFlavour);
    m_trktree->Branch("trackJetHadronConeExclTruthLabel",&trackJetHadronConeExclTruthLabel);
  }

  if(hasPixelNN){
    m_trktree->Branch("IBL_NN_phiBS",&IBL_NN_phiBS);    
    m_trktree->Branch("IBL_NN_thetaBS",&IBL_NN_thetaBS);    
    m_trktree->Branch("IBL_NN_matrixOfCharge",&IBL_NN_matrixOfCharge);    
    m_trktree->Branch("IBL_NN_vectorOfPitchesY",&IBL_NN_vectorOfPitchesY);
    m_trktree->Branch("IBL_NN_localPhiPixelIndexWeightedPosition",&IBL_NN_localPhiPixelIndexWeightedPosition);
    m_trktree->Branch("IBL_NN_localEtaPixelIndexWeightedPosition",&IBL_NN_localEtaPixelIndexWeightedPosition);

    m_trktree->Branch("BL_NN_phiBS",&BL_NN_phiBS);    
    m_trktree->Branch("BL_NN_thetaBS",&BL_NN_thetaBS);    
    m_trktree->Branch("BL_NN_matrixOfCharge",&BL_NN_matrixOfCharge);    
    m_trktree->Branch("BL_NN_vectorOfPitchesY",&BL_NN_vectorOfPitchesY);
    m_trktree->Branch("BL_NN_localPhiPixelIndexWeightedPosition",&BL_NN_localPhiPixelIndexWeightedPosition);
    m_trktree->Branch("BL_NN_localEtaPixelIndexWeightedPosition",&BL_NN_localEtaPixelIndexWeightedPosition);

  }

  if(hasRefit){
    m_trktree->Branch("trackRefitPt",&trackRefitPt);
    m_trktree->Branch("trackRefitD0",&trackRefitD0);
    m_trktree->Branch("trackRefitD0Err",&trackRefitD0Err);
    m_trktree->Branch("trackRefitZ0",&trackRefitZ0);
    m_trktree->Branch("trackRefitZ0Err",&trackRefitZ0Err);
    m_trktree->Branch("trackRefitZ0SinTheta",&trackRefitZ0SinTheta);
    m_trktree->Branch("trackRefitZ0SinThetaErr",&trackRefitZ0SinThetaErr);
    m_trktree->Branch("trackRefitNPixelHits",&trackRefitNPixelHits);
    m_trktree->Branch("trackRefitNPixelLayers",&trackRefitNPixelLayers);
  }

  m_trktree->Branch("trackPixeldEdx",&pixeldEdx);
  m_trktree->Branch("trackNPixeldEdx",&nPixeldEdx);

  m_trktree->Branch("trueD0",&trueD0);
  m_trktree->Branch("trueZ0",&trueZ0);
  m_trktree->Branch("truePhi",&truePhi);
  m_trktree->Branch("trueTheta",&trueTheta);
  m_trktree->Branch("trueqOverP",&trueqOverP);
  m_trktree->Branch("truePdgId",&truepdgid);
  m_trktree->Branch("trueBarcode",&truebarcode);
  m_trktree->Branch("truthMatchProb",&trackMatchProb);

  m_trktree->Branch("genVtxR",&genVtxR);
  m_trktree->Branch("genVtxZ",&genVtxZ);
  m_trktree->Branch("parentFlavour",&parentFlav);

  m_trktree->Branch("minTrkdR",&mindR);
  m_trktree->Branch("hitSplitIBL",&hitSplitIBL);
  m_trktree->Branch("hitSplitBL",&hitSplitBL);
  m_trktree->Branch("hitSplitL2",&hitSplitL2);
  m_trktree->Branch("hitSplitL3",&hitSplitL3);
 
  m_trktree->Branch("hitIsEndCap",&hitIsEndCap);
  m_trktree->Branch("hitIsHole",&hitIsHole);
  m_trktree->Branch("hitIsOutlier",&hitIsOutlier);
  m_trktree->Branch("hitIs3D",&hitIs3D);
  m_trktree->Branch("hitIsIBL",&hitIsIBL);
  m_trktree->Branch("hitLayer",&hitLayer);
  m_trktree->Branch("hitCharge",&hitCharge);
  m_trktree->Branch("hitToT",&hitToT);
  m_trktree->Branch("hitLVL1A",&hitLVL1A);
  m_trktree->Branch("hitNPixel",&hitNPixel);
  m_trktree->Branch("hitNPixelX",&hitNPixelX);
  m_trktree->Branch("hitNPixelY",&hitNPixelY);
  m_trktree->Branch("hitEtaModule",&hitEtaModule);
  m_trktree->Branch("hitPhiModule",&hitPhiModule);
  m_trktree->Branch("hitVBias",&hitVBias);
  m_trktree->Branch("hitVDep",&hitVDep);
  m_trktree->Branch("hitTemp",&hitTemp);
  m_trktree->Branch("hitLorentzShift",&hitLorentzShift);
  //  m_trktree->Branch("hitTanLorentzAngle",&hitTanLorentzAngle);
  m_trktree->Branch("hitIsSplit",&hitIsSplit);
  m_trktree->Branch("hitSplitProb1",&hitSplitProb1);
  m_trktree->Branch("hitSplitProb2",&hitSplitProb2);
  //  m_trktree->Branch("hitSplitProb3",&hitSplitProb3);
  m_trktree->Branch("hitGlobalX",&hitGlobalX);
  m_trktree->Branch("hitGlobalY",&hitGlobalY);
  m_trktree->Branch("hitGlobalZ",&hitGlobalZ);
  m_trktree->Branch("hitLocalX",&hitLocalX);
  m_trktree->Branch("hitLocalY",&hitLocalY);

  m_trktree->Branch("clusterLocalX",&clustLocalX);
  m_trktree->Branch("clusterLocalY",&clustLocalY);

  m_trktree->Branch("g4LocalX",&g4LocalX);
  m_trktree->Branch("g4LocalY",&g4LocalY);
  m_trktree->Branch("g4EntryLocalX",&g4EntryLocalX);
  m_trktree->Branch("g4EntryLocalY",&g4EntryLocalY);
  m_trktree->Branch("g4ExitLocalX",&g4ExitLocalX);
  m_trktree->Branch("g4ExitLocalY",&g4ExitLocalY);
  m_trktree->Branch("g4Barcode",&g4Barcode);
  m_trktree->Branch("g4PdgId",&g4PdgId);
  m_trktree->Branch("g4EnergyDeposit",&g4EnergyDeposit);

  m_trktree->Branch("trkLocalX",&trkLocalX);
  m_trktree->Branch("trkLocalY",&trkLocalY);
  m_trktree->Branch("hitLocalErrorX",&hitLocalErrorX);
  m_trktree->Branch("hitLocalErrorY",&hitLocalErrorY);
  m_trktree->Branch("trkLocalErrorX",&trkLocalErrorX);
  m_trktree->Branch("trkLocalErrorY",&trkLocalErrorY);
  m_trktree->Branch("unbiasedResidualX",&unbiasedResidualX);
  m_trktree->Branch("unbiasedResidualY",&unbiasedResidualY);
  m_trktree->Branch("biasedResidualX",&biasedResidualX);
  m_trktree->Branch("biasedResidualY",&biasedResidualY);
  m_trktree->Branch("unbiasedPullX",&unbiasedPullX);
  m_trktree->Branch("unbiasedPullY",&unbiasedPullY);
  m_trktree->Branch("biasedPullX",&biasedPullX);
  m_trktree->Branch("biasedPullY",&biasedPullY);

  if(hasRefit){
    m_trktree->Branch("trkRefitLocalX",&trkRefitLocalX);
    m_trktree->Branch("trkRefitLocalY",&trkRefitLocalY);
    m_trktree->Branch("unbiasedResidualRefitX",&unbiasedResidualRefitX);
    m_trktree->Branch("unbiasedResidualRefitY",&unbiasedResidualRefitY);
    m_trktree->Branch("biasedResidualRefitX",&biasedResidualRefitX);
    m_trktree->Branch("biasedResidualRefitY",&biasedResidualRefitY);
    m_trktree->Branch("unbiasedResidualRefitXErr",&unbiasedResidualRefitXErr);
    m_trktree->Branch("unbiasedResidualRefitYErr",&unbiasedResidualRefitYErr);
    m_trktree->Branch("biasedResidualRefitXErr",&biasedResidualRefitXErr);
    m_trktree->Branch("biasedResidualRefitYErr",&biasedResidualRefitYErr);
  }

  m_trktree->Branch("trk2G4DistX",&minTrkG4DistX);
  m_trktree->Branch("trk2G4DistY",&minTrkG4DistY);

  m_trktree->Branch("trk2DistX",&minTrkDistX);
  m_trktree->Branch("trk2DistY",&minTrkDistY);

  m_trktree->Branch("trkDens10",&trkDens10);
  m_trktree->Branch("trkDens25",&trkDens25);
  m_trktree->Branch("trkDens50",&trkDens50);
  m_trktree->Branch("trkDensPitch2",&trkDensPitch2);
  m_trktree->Branch("trkDensPitch5",&trkDensPitch5);
  m_trktree->Branch("trkDensPitch10",&trkDensPitch10);
  m_trktree->Branch("hitDens10",&hitDens10);
  m_trktree->Branch("hitDens25",&hitDens25);
  m_trktree->Branch("hitDens50",&hitDens50);

  m_trktree->Branch("hit3DClusterDX",&hit3DClusterDX);
  m_trktree->Branch("hit3DClusterDY",&hit3DClusterDY);

  m_trktree->Branch("trkPhiOnSurface",&trkPhiOnSurface);
  m_trktree->Branch("trkThetaOnSurface",&trkThetaOnSurface);

  m_trktree->Branch("trkHitLocalX",&trkHitLocalX);
  m_trktree->Branch("trkHitLocalY",&trkHitLocalY);
  m_trktree->Branch("trkHitLocalErrorX",&trkHitLocalErrorX);
  m_trktree->Branch("trkHitLocalErrorY",&trkHitLocalErrorY);

  m_trktree->Branch("pixelClusterLayer",&pixelLayer);
  m_trktree->Branch("pixelClusterIsSplit",&pixelIsSplit);
  m_trktree->Branch("pixelClusterLocalX",&pixelLocalX);
  m_trktree->Branch("pixelClusterLocalY",&pixelLocalY);
  m_trktree->Branch("pixelClusterLocalErrorX",&pixelLocalErrorX);
  m_trktree->Branch("pixelClusterLocalErrorY",&pixelLocalErrorY);

  m_trktree->Branch("hitNContributingPtcs",&clus_nContributingPtcs);
  m_trktree->Branch("hitNContributingPU",&clus_nContributingPU);
  m_trktree->Branch("hitContributingPtcsBarcode",&clus_contributingPtcsBarcode);
  m_trktree->Branch("hitContributingPtcsPdgId",&clus_contributingPtcsPdgId);
  m_trktree->Branch("hitContributingPtcsE",&clus_contributingPtcsE);
  m_trktree->Branch("hitContributingPtcsPt",&clus_contributingPtcsPt);

  m_trktree->Branch("pixelIBLIndex",&clus_pixelIBLIndex);
  m_trktree->Branch("pixelIBLRow",&clus_pixelIBLRow);
  m_trktree->Branch("pixelIBLCol",&clus_pixelIBLCol);
  m_trktree->Branch("pixelIBLCharge",&clus_pixelIBLCharge);
  m_trktree->Branch("pixelIBLToT",&clus_pixelIBLToT);

  m_trktree->Branch("pixelBLIndex",&clus_pixelBLIndex);
  m_trktree->Branch("pixelBLRow",&clus_pixelBLRow);
  m_trktree->Branch("pixelBLCol",&clus_pixelBLCol);
  m_trktree->Branch("pixelBLCharge",&clus_pixelBLCharge);
  m_trktree->Branch("pixelBLToT",&clus_pixelBLToT);

  m_trktree->Branch("pixelL2Index",&clus_pixelL2Index);
  m_trktree->Branch("pixelL2Row",&clus_pixelL2Row);
  m_trktree->Branch("pixelL2Col",&clus_pixelL2Col);
  m_trktree->Branch("pixelL2Charge",&clus_pixelL2Charge);
  m_trktree->Branch("pixelL2ToT",&clus_pixelL2ToT);

  m_trktree->Branch("pixelL3Index",&clus_pixelL3Index);
  m_trktree->Branch("pixelL3Row",&clus_pixelL3Row);
  m_trktree->Branch("pixelL3Col",&clus_pixelL3Col);
  m_trktree->Branch("pixelL3Charge",&clus_pixelL3Charge);
  m_trktree->Branch("pixelL3ToT",&clus_pixelL3ToT);


  TFile *outputSplitTrkFile = wk()->getOutputFile("splittrktree");
  m_splittrktree = new TTree("splittrktree","splittrktree");
  m_splittrktree->SetDirectory(outputSplitTrkFile);
  m_splittrktree->Branch("runNumber",&m_runNumber);
  m_splittrktree->Branch("eventNumber",&m_eventNumber);
  m_splittrktree->Branch("timeStamp",&m_timeStamp);
  m_splittrktree->Branch("lumiBlock",&lumiB);
  m_splittrktree->Branch("averagePU",&m_averagePU);
  m_splittrktree->Branch("eventPU",&m_eventPU);
  m_splittrktree->Branch("mcFlag",&mcFlag);
  
  m_splittrktree->Branch("trackNPixelHits",&nPixelHits);
  m_splittrktree->Branch("trackNPixelLayers",&nPixelLayers);
  m_splittrktree->Branch("trackNIBLHits",&nIBLHits);
  m_splittrktree->Branch("trackNBLHits",&nBLHits);
  m_splittrktree->Branch("trackNL2Hits",&nL2Hits);
  m_splittrktree->Branch("trackNL3Hits",&nL3Hits);
  m_splittrktree->Branch("trackNECHits",&nECHits);
  m_splittrktree->Branch("trackNSCTHits",&nSCTHits);
  m_splittrktree->Branch("trackNTRTHits",&nTRTHits);
  
  m_splittrktree->Branch("trackNPixelHits2",&nPixelHits2);
  m_splittrktree->Branch("trackNSCTHits2",&nSCTHits2);
  m_splittrktree->Branch("trackNIBLHits2",&nIBLHits2);
  m_splittrktree->Branch("trackNBLHits2",&nBLHits2);

  m_splittrktree->Branch("trackCharge",&splitTrackCharge);
  m_splittrktree->Branch("trackPt",&splitTrackPt);
  m_splittrktree->Branch("trackPhi",&splitTrackPhi);
  m_splittrktree->Branch("trackEta",&splitTrackEta);
  m_splittrktree->Branch("trackTheta",&splitTrackTheta);
  m_splittrktree->Branch("trackqOverP",&splitTrackqOverP);
  m_splittrktree->Branch("trackD0",&splitTrackD0);
  m_splittrktree->Branch("trackZ0",&splitTrackZ0);
  m_splittrktree->Branch("trackD0Err",&splitTrackD0Err);
  m_splittrktree->Branch("trackZ0Err",&splitTrackZ0Err);
  m_splittrktree->Branch("trackqOverPErr",&splitTrackqOverPErr);
  
  m_splittrktree->Branch("trackMissDistRPhi",&missDistRPhi);
  m_splittrktree->Branch("trackMissDistZ",&missDistZ);

  m_fileCounter = 0;

  nIntEC = 0;
  nIntIBL = 0;
  nIntBL = 0;
  nIntL1 = 0;
  nIntL2 = 0;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackxAODAnalysis :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackxAODAnalysis :: changeInput (bool firstFile)
{

  if(firstFile){
    Info("firstInput", "File number = %i", m_fileCounter );
  }else{
    Info("changeInput", "File number = %i", m_fileCounter );
  }
  m_fileCounter++;
  Info("changeInput", "Processing file \"%s\"",wk()->inputFile()->GetName());
  Info("changeInput", "This file has %lli entries", wk()->xaodEvent()->getEntries());

  iblMCRadHV = -1;
  ibl3dMCRadHV = -1;
  blMCRadHV = -1;
  iblP2Cut = 0.;
  iblP3Cut = 0.;

  const char * thisFileName = wk()->inputFile()->GetName();
  
  std::string stringFileName(thisFileName);

  //HV
 
  std::string stringIBLPL = "IBLPL";
  std::string stringIBL3D = "IBL3D";
  std::size_t foundIBLPL = stringFileName.find(stringIBLPL);
  std::size_t foundIBL3D = stringFileName.find(stringIBL3D);


  std::string stringHV1 = "V.DAOD";
  std::string stringHV2 = "V.Beta";
  std::string stringHV3 = "V.IBL";

  std::size_t foundHV = stringFileName.find(stringHV1);
  if(foundHV<=0||foundHV>1000){
    foundHV = stringFileName.find(stringHV2);  
  }
  std::size_t foundHV3 = stringFileName.find(stringHV3);
 
  /*
  std::string stringHV3D = "V.DAOD";
 
  std::size_t foundHV3D = stringFileName.find(stringHV3D);
  */
  /*
  std::string stringHVBL = "VBL.DAOD_IDTRKVALID";
 
  std::size_t foundHVBL = stringFileName.find(stringHVBL);
  */
  //  std::cout << foundHV << std::endl;  
  char charHV[3];
  /*
  std::cout << foundHVBL << std::endl;  
  char charHVBL[3];
  */
  /*
  std::cout << foundHV3D << std::endl;  
  char charHV3D[3];
  */

  if(foundIBLPL>0 && foundIBLPL <1000 && foundHV>0 && foundHV<1000){
    
    strncpy(charHV,thisFileName+foundHV-3,3);
    
    std::string strHV(charHV);
    
    iblMCRadHV = std::stof(strHV);
    
    std::cout << "MCRad IBL HV Setting " << iblMCRadHV << " V" << std::endl;
  }
  
  if(foundIBL3D>0 && foundIBL3D <1000 && foundHV3>0 && foundHV3<1000){

    strncpy(charHV,thisFileName+foundHV3-2,3);
    
    std::string strHV3(charHV);
    
    ibl3dMCRadHV = std::stof(strHV3);
    
    std::cout << "MCRad IBL 3D HV Setting " << ibl3dMCRadHV << " V" << std::endl;
  } else if(foundIBL3D>0 && foundIBL3D <1000 && foundHV>0 && foundHV<1000){

    strncpy(charHV,thisFileName+foundHV-2,3);
    
    std::string strHV(charHV);
    
    ibl3dMCRadHV = std::stof(strHV);
    
    std::cout << "MCRad IBL 3D HV Setting " << ibl3dMCRadHV << " V" << std::endl;  
  }

  //Split P Scan
  std::string stringPScan = "lwtnnRel21_ScanV0_";
 
  std::size_t foundPScan = stringFileName.find(stringPScan);

  std::cout << foundPScan << std::endl;  
  char charP2Scan[3];
  char charP3Scan[3];
  
  /*
  if(foundHV>0&&foundHV<1000){  
    strncpy(charHV,thisFileName+foundHV-3,3);
    std::string strHV(charHV);
    iblMCRadHV =std::stof(strHV);
    std::cout << "MCRad IBL HV Setting " << iblMCRadHV << " V" << std::endl;
  }
  */
  /*
  if(foundHVBL>0&&foundHVBL<1000){  
    strncpy(charHVBL,thisFileName+foundHVBL-3,3);
    std::string strHVBL(charHVBL);
    blMCRadHV =std::stof(strHVBL);
    std::cout << "MCRad BL HV Setting " << blMCRadHV << " V" << std::endl;
  }
  */
  /*
  if(foundHV3D>0&&foundHV3D<1000){  
    strncpy(charHV3D,thisFileName+foundHV3D-3,3);
    std::string strHV3D(charHVBL);
    ibl3dMCRadHV =std::stof(strHV3D);
    std::cout << "MCRad 3D HV Setting " << ibl3dMCRadHV << " V" << std::endl;
  }
  */
  if(foundPScan>0&&foundPScan<500){  
    strncpy(charP2Scan,thisFileName+foundPScan+18,sizeof(charP2Scan));
    iblP2Cut = std::stof(charP2Scan);
    int ioff = 22;
    if(iblP2Cut==0.)ioff = 20;
    strncpy(charP3Scan,thisFileName+foundPScan+ioff,sizeof(charP3Scan));
    iblP3Cut = std::stof(charP3Scan);
    std::cout << "IBL Split Prob Cuts: P2 " << iblP2Cut << "; P3 " << iblP3Cut << std::endl;
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackxAODAnalysis :: initialize ()
{
  m_event = wk()->xaodEvent();
  m_eventCounter = 0;

  isMC = false;
  isCalibration = false;
  const xAOD::EventInfo* eventInfo = 0;
  
  if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
    Error("execute()", "Failed to retrieve event info collection. Exiting." );
    return EL::StatusCode::FAILURE;
  }
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; 
  }else if(eventInfo->eventType( xAOD::EventInfo::IS_CALIBRATION ) ){
    isCalibration = true; 
  }

  /*
  ANA_CHECK( m_trigConfTool_handle.setProperty("OutputLevel", msg().level()));
  ANA_CHECK( m_trigConfTool_handle.retrieve());
  ANA_MSG_DEBUG("Retrieved tool: " << m_trigConfTool_handle);
  ANA_CHECK( m_trigDecTool_handle.setProperty( "ConfigTool", m_trigConfTool_handle ));
  ANA_CHECK( m_trigDecTool_handle.setProperty( "TrigDecisionKey", "xTrigDecision" ));
  ANA_CHECK( m_trigDecTool_handle.setProperty( "OutputLevel", msg().level() ));
  ANA_CHECK( m_trigDecTool_handle.retrieve());
  ANA_MSG_DEBUG("Retrieved tool: " << m_trigDecTool_handle);
  */
  for(int i=0;i<m_trig_cnf;i++){
    m_HLT_Trig_count[i] = 0;
  }

  Info("initialize()", "Number of events = %lli", m_event->getEntries() ); 
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackxAODAnalysis :: execute ()
{

  if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  
  if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
    Error("execute()", "Failed to retrieve event info collection. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; // can do something with this later
  }else{
    isMC = false;
    hasSiHits = false;
  }

  m_runNumber=eventInfo->runNumber();
  m_eventNumber=eventInfo->eventNumber();
  m_timeStamp=eventInfo->timeStamp();
  m_averagePU=eventInfo->averageInteractionsPerCrossing();
  m_eventPU=eventInfo->actualInteractionsPerCrossing();

  //  std::cout <<  m_runNumber << " " <<  m_eventNumber << " " <<  m_averagePU << " " <<  m_eventPU << std::endl;

  //======================================================
  // Get trigger info
  //======================================================

  //Triggers
  int m_Num_HLT_Conf = 11;
  m_HLT_Trig_name[0] = "HLT_j100";
  m_HLT_Trig_name[1] = "HLT_j150";
  m_HLT_Trig_name[2] = "HLT_j200";
  m_HLT_Trig_name[3] = "HLT_j300";
  m_HLT_Trig_name[4] = "HLT_j320";
  m_HLT_Trig_name[5] = "HLT_j360";
  m_HLT_Trig_name[6] = "HLT_j380";
  m_HLT_Trig_name[7] = "HLT_j400";
  m_HLT_Trig_name[8] = "HLT_j420";
  m_HLT_Trig_name[9] = "HLT_j460";
  m_HLT_Trig_name[10] = "noalg_L1MBTS_1";

  m_HLT_j100 = false;
  m_HLT_j150 = false;
  m_HLT_j200 = false;
  m_HLT_j300 = false;
  m_HLT_j320 = false;
  m_HLT_j360 = false;
  m_HLT_j380 = false;
  m_HLT_j400 = false;
  m_HLT_j420 = false;
  m_HLT_j460 = false;
  m_HLT_MBTS = false;

  /*
  for (int i=0; i<m_Num_HLT_Conf; ++i) {
    std::ostringstream ostr_name;
    ostr_name << m_HLT_Trig_name[i];
    std::string trig_name = ostr_name.str();
    m_HLT_Trig_fire[i] = m_trigDecTool_handle->isPassed( trig_name );
    if( m_HLT_Trig_fire[i])m_HLT_Trig_count[i]++;
  }
  */

  m_HLT_j100 = m_HLT_Trig_fire[0];
  m_HLT_j150 = m_HLT_Trig_fire[1];
  m_HLT_j200 = m_HLT_Trig_fire[2];
  m_HLT_j300 = m_HLT_Trig_fire[3];
  m_HLT_j320 = m_HLT_Trig_fire[4];
  m_HLT_j360 = m_HLT_Trig_fire[5];
  m_HLT_j380 = m_HLT_Trig_fire[6];
  m_HLT_j400 = m_HLT_Trig_fire[7];
  m_HLT_j420 = m_HLT_Trig_fire[8];
  m_HLT_j460 = m_HLT_Trig_fire[9];
  m_HLT_MBTS = m_HLT_Trig_fire[10];


  //Jets 

  //  static const char* JetCollectionName = "AntiKt4EMTopoJets";  
  const xAOD::JetContainer* jets = 0;
  if ( doJets && !m_event->retrieve( jets, JetCollectionName ).isSuccess()){ 
    Error("execute()", "Failed to retrieve Jet container. Exiting." );
    return EL::StatusCode::FAILURE;
  }


  // Track container
  const xAOD::TrackParticleContainer* recoTracks = 0;
  if ( !m_event->retrieve( recoTracks, "InDetTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    // if ( !m_event->retrieve( recoTracks, "InDetIdealTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    // if ( !m_event->retrieve( recoTracks, "InDetForwardTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    // if ( !m_event->retrieve( recoTracks, "InDetPixelPrdAssociationTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    //  if ( !m_event->retrieve( recoTracks, "InDetSplitTrackParticles" ).isSuccess() ){

    Error("execute()", "Failed to retrieve Reconstructed Track container. Exiting." );
    return EL::StatusCode::FAILURE;
  }else{
    if(m_eventCounter < 10){
      std::cout << "Retrieved Track container with " << recoTracks->size() << " tracks " << std::endl;
    }
  }

  nbOfTracks = recoTracks->size();

  // Split Track container
  const xAOD::TrackParticleContainer* recoSplitTracks = 0;
  if ( doSplitTracks && !m_event->retrieve( recoSplitTracks, "InDetSplitTrackParticles" ).isSuccess() ){
    Error("execute()", "Failed to retrieve Reconstructed Split Track container. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  nbOfFwdTracks = -1;
  /*
  const xAOD::TrackParticleContainer* recoFwdTracks = 0;
  if ( !m_event->retrieve( recoFwdTracks, "InDetForwardTrackParticles" ).isSuccess() ){ 
    Error("execute()", "Failed to retrieve Reconstructed Fwd Track container. Exiting." );
  }else{
    nbOfFwdTracks = recoFwdTracks->size();
    if(m_eventCounter < 10){
      std::cout << "Retrieved Fwd Track container with " << recoFwdTracks->size() << " tracks " << std::endl;
    }
  }
  */

  nPVtx = 0;
  pVtxNTrk = 0;
  pVtxX = -999.;
  pVtxY = -999.;
  pVtxZ = -999.;
  pVtxXErr = -999.;
  pVtxYErr = -999.;
  pVtxZErr = -999.;

  bool havePVtx = false;

  if(!isCosmic && !isCalibration){
  //  if(!isCalibration){
    const xAOD::VertexContainer* recoVertices(nullptr);
    if(m_event->retrieve( recoVertices, "PrimaryVertices").isSuccess()){
      for( auto recoVtx_itr = recoVertices->begin(); recoVtx_itr != recoVertices->end(); recoVtx_itr++) {
	if((*recoVtx_itr)->vertexType() == xAOD::VxType::PriVtx){
	  if(!havePVtx){
	    const Amg::MatrixX& pVtxCov = (*recoVtx_itr)->covariancePosition();
	    if( (*recoVtx_itr)->isAvailable< float >( "x" ) ){
	      pVtxX = (*recoVtx_itr)->x();
	      pVtxXErr = sqrt(pVtxCov(0,0));
	    }
	    if( (*recoVtx_itr)->isAvailable< float >( "y" ) ){
	      pVtxY = (*recoVtx_itr)->y();
	      pVtxYErr = sqrt(pVtxCov(1,1));
	    }
	    if( (*recoVtx_itr)->isAvailable< float >( "z" ) ){
	      pVtxZ = (*recoVtx_itr)->z();
	      pVtxZErr = sqrt(pVtxCov(2,2));
	    }
	    pVtxNTrk = (*recoVtx_itr)->nTrackParticles();
	    //	  break;
	    havePVtx = true;
	  }
	}
	if((*recoVtx_itr)->nTrackParticles()>=2)nPVtx++;
      }
    }
  }

  const xAOD::TruthParticleContainer* truthPtcs = 0;
  const xAOD::TruthVertexContainer* truthVertices(nullptr);

  truthPVtxX = -999.;
  truthPVtxY = -999.;
  truthPVtxZ = -999.;

  if(isMC){
    // get particle container of interest
    if ( !m_event->retrieve( truthPtcs, "TruthParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
      Error("execute()", "Failed to retrieve Truth Particle container. Exiting." );
      return EL::StatusCode::FAILURE;
    }
    
    if(m_event->retrieve( truthVertices, "TruthVertices").isSuccess()){
      for( auto truthVtx_itr = truthVertices->begin(); truthVtx_itr != truthVertices->end(); truthVtx_itr++) {
	//	if((*truthVtx_itr)->vertexType() == xAOD::VxType::PriVtx){
	  if( (*truthVtx_itr)->isAvailable< float >( "x" ) ) truthPVtxX = (*truthVtx_itr)->x();
	  if( (*truthVtx_itr)->isAvailable< float >( "y" ) ) truthPVtxX = (*truthVtx_itr)->y();
	  if( (*truthVtx_itr)->isAvailable< float >( "z" ) ) truthPVtxX = (*truthVtx_itr)->z();
	break;
	//      }
      }
    }
  }
  if(isMC & doGenTree){
      // loop over the truth particles in the container
    for( auto truthPtc_itr = truthPtcs->begin(); truthPtc_itr != truthPtcs->end(); truthPtc_itr++) {
      
      truthPt = -999;
      truthPdgId = -999;
      truthTheta = -999;
      trueD0=-999.;
      trueZ0=-999.;
      genVtxR = 0;
      genVtxZ = 0;
      
      if((*truthPtc_itr)->pdgId()==25)bosonPt=(*truthPtc_itr)->pt()/1000.;

      if ( (*truthPtc_itr)->pt()/1000. < minTrackPt || !(*truthPtc_itr)->isCharged() ||  (*truthPtc_itr)->abseta() > 2.5 || (*truthPtc_itr)->status() != 1) continue;
      
      truthPt  = (*truthPtc_itr)->pt()/1000.;
      truthEta = (*truthPtc_itr)->eta();
      truthCharge = (*truthPtc_itr)->charge();
      truthPdgId = (*truthPtc_itr)->pdgId();
      
      if(  (*truthPtc_itr)->isAvailable< float >( "d0" )){
	trueD0 =  (*truthPtc_itr)->auxdataConst< float >( "d0" );
      }else{
	trueD0 = -999;
      }
      if(  (*truthPtc_itr)->isAvailable< float >( "z0" )){
	trueZ0 =  (*truthPtc_itr)->auxdataConst< float >( "z0" );
      }else{
	trueZ0 = -999;
      }
      
      if((*truthPtc_itr)->hasProdVtx() && !hasPixelNN){
	  genVtxR = sqrt((*truthPtc_itr)->prodVtx()->x()* (*truthPtc_itr)->prodVtx()->x() +  (*truthPtc_itr)->prodVtx()->y()* (*truthPtc_itr)->prodVtx()->y());
	  genVtxZ =  (*truthPtc_itr)->prodVtx()->z();
      }else{
	genVtxR = 0;
	genVtxZ = 0;
      }
      
      parentFlav = -1;
      
      for(int ip=0;ip<(int)(*truthPtc_itr)->nParents();ip++){

	if((*truthPtc_itr)->parent(ip)->isStrangeHadron()){
	  if(parentFlav<3)parentFlav = 3;
	}else if((*truthPtc_itr)->parent(ip)->isCharmHadron()){   
	  if(parentFlav<4)parentFlav = 4;
	}else if((*truthPtc_itr)->parent(ip)->isBottomHadron()){  
	  if(parentFlav<5)parentFlav = 5;
	}
	if(abs((*truthPtc_itr)->parent(ip)->pdgId())==5 || abs((*truthPtc_itr)->parent(ip)->pdgId())==511 || abs((*truthPtc_itr)->parent(ip)->pdgId())==521 || abs((*truthPtc_itr)->parent(ip)->pdgId())==531)parentFlav = 5;
      }
      
      mindR = 9999.;
      
      for( auto truthNextPtc_itr = truthPtcs->begin(); truthNextPtc_itr != truthPtcs->end(); truthNextPtc_itr++) {
	
	if((*truthNextPtc_itr)==(*truthPtc_itr) || (*truthNextPtc_itr)->pt() < 400 || !(*truthNextPtc_itr)->isCharged() ||  (*truthNextPtc_itr)->abseta() > 2.75 || (*truthNextPtc_itr)->status() != 1) continue;
	
	float dEta = (*truthNextPtc_itr)->eta() - (*truthPtc_itr)->eta();
	float dPhi = (*truthNextPtc_itr)->phi() - (*truthPtc_itr)->phi();
	float dr =  sqrtf(dEta*dEta + dPhi*dPhi);
	if(dr<mindR && dr>1.e-3) {mindR = dr;}
      }
      
      trackPt  = -999;
      trackEta = -999;
      trackPhi = -999;
      trackTheta = -999;
      trackD0 = -999;
      trackZ0 = -999;
      trackD0PV = -999;
      trackZ0PV = -999;
      trackRefX = -999;
      trackRefY = -999;
      trackRefZ = -999;
      trackDeltaZSinTheta = -999;
      nPixelLayers = -1;
      nPixelHits = -1;
      nPixelHoles = -1;
      nPixelOutliers = -1;
      nSCTHits = -1;
      nSCTHoles = -1;
      nTRTHits = -1;
      nIBLHits = -1;
      nBLHits = -1;
      trackMatchProb = -1;
      trackJetdR = -999.;
      trackJetPt = -999.;
      trackJetFlavour = -999;
      trackJetHadronConeExclTruthLabel = -999;

      // loop over the tracks in the container
      // auto type: xAOD::TrackParticleContainer::const_iterator
      for( auto recoTrk_itr = recoTracks->begin(); recoTrk_itr != recoTracks->end(); recoTrk_itr++) {

	typedef ElementLink< xAOD::TruthParticleContainer > Link_t;
	static const char* NAME = "truthParticleLink";
	if( (*recoTrk_itr)->isAvailable< Link_t >( NAME ) ) {
	  const Link_t& link = (*recoTrk_itr)->auxdata< Link_t >( NAME );
	  if( link.isValid() ) {
	    xAOD::TruthParticle *truthParticle = (xAOD::TruthParticle*) *link;
	    if(truthParticle->barcode() == (*truthPtc_itr)->barcode()){

	      trackPt  =  (*recoTrk_itr)->pt()/1000.;
	      trackEta =  (*recoTrk_itr)->eta();
	      trackTheta =  (*recoTrk_itr)->theta();
	      trackPhi =  (*recoTrk_itr)->phi();
	      trackD0  = (*recoTrk_itr)->d0();
	      trackZ0  = (*recoTrk_itr)->z0();
	      trackRefX  = (*recoTrk_itr)->vx();
	      trackRefY  = (*recoTrk_itr)->vy();
	      trackRefZ  = (*recoTrk_itr)->vz();
	      trackChi2 = (*recoTrk_itr)->chiSquared();
	      trackNdof = (*recoTrk_itr)->numberDoF();

	      trackDeltaZSinTheta = ((*recoTrk_itr)->z0()+((*recoTrk_itr)->vz())-pVtxZ) * sin((*recoTrk_itr)->theta());

	      //	      std::cout << (*recoTrk_itr)->z0() << " " << (*recoTrk_itr)->vz() << " " << pVtxZ << " " <<  sin((*recoTrk_itr)->theta()) << " " << trackDeltaZSinTheta << std::endl;

	      if(isMC){
		trackMatchProb = (*recoTrk_itr)->auxdata< float >( "truthMatchProbability" );
	      }else{
		trackMatchProb = -1.;
	      }

	      uint8_t numberOfPixelLayers= 0;
	      uint8_t numberOfSCTHits= 0;
	      uint8_t numberOfTRTHits= 0;
	      uint8_t numberOfPixelHits= 0;
	      uint8_t numberOfIBLHits= 0;
	      uint8_t numberOfBLHits= 0;
	      uint8_t numberOfPixelHoles= 0;
	      uint8_t numberOfPixelOutliers= 0;
	      uint8_t numberOfSCTHoles= 0;

	      uint8_t numberOfIBLSplitHits= 0;
	      uint8_t numberOfIBLSharedHits= 0;
	      uint8_t numberOfBLSplitHits= 0;
	      uint8_t numberOfBLSharedHits= 0;
	      
	      uint8_t numberOfIBLExpectedHits= 0;
	      uint8_t numberOfBLExpectedHits= 0;
	      uint8_t numberOfSCTSharedHits= 0;
	      
	      if(  ! (*recoTrk_itr)->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits)) 
		Error("passTrackQualitySelection()", "Failed to retrieved summaryValue Pixel Hits" );
	      if(  ! (*recoTrk_itr)->summaryValue(numberOfSCTHits,xAOD::numberOfSCTHits))
		Error("passTrackQualitySelection()", "Failed to retrieved summaryValue SCT hits" );
	      if(  ! (*recoTrk_itr)->summaryValue(numberOfTRTHits,xAOD::numberOfTRTHits))
		Error("passTrackQualitySelection()", "Failed to retrieved summaryValue TRT hits" );
	      (*recoTrk_itr)->summaryValue(numberOfPixelLayers,xAOD::numberOfContribPixelLayers);
	      (*recoTrk_itr)->summaryValue(numberOfIBLHits,xAOD::numberOfInnermostPixelLayerHits);
	      (*recoTrk_itr)->summaryValue(numberOfBLHits,xAOD::numberOfNextToInnermostPixelLayerHits);
	      (*recoTrk_itr)->summaryValue(numberOfPixelHoles,xAOD::numberOfPixelHoles);
	      (*recoTrk_itr)->summaryValue(numberOfPixelOutliers,xAOD::numberOfPixelOutliers);
	      (*recoTrk_itr)->summaryValue(numberOfSCTHoles,xAOD::numberOfSCTHoles);

	      (*recoTrk_itr)->summaryValue(numberOfIBLSplitHits,xAOD::numberOfInnermostPixelLayerSplitHits);
	      (*recoTrk_itr)->summaryValue(numberOfIBLSharedHits,xAOD::numberOfInnermostPixelLayerSharedHits);
	      (*recoTrk_itr)->summaryValue(numberOfBLSplitHits,xAOD::numberOfNextToInnermostPixelLayerSplitHits);
	      (*recoTrk_itr)->summaryValue(numberOfBLSharedHits,xAOD::numberOfNextToInnermostPixelLayerSharedHits);

	      (*recoTrk_itr)->summaryValue(numberOfIBLExpectedHits,xAOD::expectInnermostPixelLayerHit);
	      (*recoTrk_itr)->summaryValue(numberOfBLExpectedHits,xAOD::expectNextToInnermostPixelLayerHit);
	      (*recoTrk_itr)->summaryValue(numberOfSCTSharedHits,xAOD::numberOfSCTSharedHits);
 
	      nPixelLayers = numberOfPixelLayers;
	      nPixelHits = numberOfPixelHits;
	      nPixelHoles = numberOfPixelHoles;
	      nPixelOutliers = numberOfPixelOutliers;
	      nSCTHits = numberOfSCTHits;
	      nSCTHoles = numberOfSCTHoles;
	      nTRTHits = numberOfTRTHits;
	      nIBLHits = numberOfIBLHits;
	      nBLHits = numberOfBLHits;

	      nIBLSplitHits = numberOfIBLSplitHits;
	      nBLSplitHits = numberOfBLSplitHits;
	      nIBLSharedHits = numberOfIBLSharedHits;
	      nBLSharedHits = numberOfBLSharedHits;
	      
	      nIBLExpectedHits = numberOfIBLExpectedHits;
	      nBLExpectedHits = numberOfBLExpectedHits;
	      nSCTSharedHits = numberOfSCTSharedHits;

	    }
	  }
	}
      }

      m_gentree->Fill();
    }
  }

  // Cluster tuple

  //Loop over the the pixel clusters
  const xAOD::TrackMeasurementValidationContainer* pixelClusters = 0;
  if ( !m_event->retrieve( pixelClusters, "PixelClusters" ).isSuccess() ){ // retrieve arguments: container type, container key
    //    Error("execute()", "Failed to retrieve Pixel Clusters. Exiting." );
    //    return EL::StatusCode::FAILURE;
    doClusterTree = false;
  }

  if(doClusterTree){

    int nEC = 0;
    int nIBL = 0;
    int nBL = 0;
    int nL1 = 0;
    int nL2 = 0;
    
    for( auto  cluster_itr = pixelClusters->begin(); cluster_itr != pixelClusters->end(); cluster_itr++){  
      
      const xAOD::TrackMeasurementValidation* pixelCluster =  *cluster_itr;

      clus_pixelIndex.clear();
      clus_pixelRow.clear();
      clus_pixelCol.clear();
      clus_pixelCharge.clear();
      clus_pixelToT.clear();
      
      int bec = (*cluster_itr)->auxdataConst< int>("bec");
      int layer = (*cluster_itr)->auxdataConst< int>("layer");
      
      if(bec!=0){
	nEC++;
      }else{
	if(layer==0){
	  nIBL++;
	}else if(layer==1){
	  nBL++;
	}else if(layer==2){
	  nL1++;
	}else if(layer==3){
	  nL2++;
	}
      }
      float pixelCharge(0);
      float pixelToT(0);
      float pixelLVL1A(0);
      std::vector<int> pixelSiHitBarcode;
      std::vector<int> pixelTruthBarcode;
      if( (*cluster_itr)->isAvailable< float >( "charge" ) )
	pixelCharge = (*cluster_itr)->auxdataConst< float >("charge"); 
      if( (*cluster_itr)->isAvailable< int >( "ToT" ) )
	pixelToT = (*cluster_itr)->auxdataConst< int >("ToT");
      if( (*cluster_itr)->isAvailable< int >( "LVL1A" ) ) 
	pixelLVL1A = (*cluster_itr)->auxdataConst<int>("LVL1A");
 
      if(isMC){
	if( (*cluster_itr)->isAvailable< std::vector<int> >( "sihit_barcode" ) ){
	  pixelSiHitBarcode = (*cluster_itr)->auxdataConst< std::vector<int> >("sihit_barcode"); 
	}else{
	  hasSiHits = false;
	}
	if( (*cluster_itr)->isAvailable< std::vector<int> >( "truth_barcode" ) ){
	  pixelTruthBarcode = (*cluster_itr)->auxdataConst< std::vector<int> >("truth_barcode"); 
	}else{
	  hasSiHits = false;
	}
      }

      clusterIsEndCap = bec;
      clusterLayer = layer;
      clusterCharge = pixelCharge;
      clusterToT = pixelToT;
      clusterLVL1A = pixelLVL1A;

      clusterIsSplit    = pixelCluster->auxdataConst< int >( "isSplit" );
      clusterSplitProb1 = pixelCluster->auxdata<float>("splitProbability1");
      clusterSplitProb2 = pixelCluster->auxdata<float>("splitProbability2");

      if( pixelCluster->isAvailable< float >( "localX" ) ){
	clusterLocalX = pixelCluster->localX();
      }else{
	clusterLocalX = -999;
      }
      if( pixelCluster->isAvailable< float >( "localY" ) ){
	clusterLocalY = pixelCluster->localY();
      }else{
	clusterLocalY = -999;
      }

      if( pixelCluster->isAvailable< float >( "globalX" ) ){
	clusterGlobalX = pixelCluster->globalX();
      }else{
	clusterGlobalX = -999;
      }
      if( pixelCluster->isAvailable< float >( "globalY" ) ){
	clusterGlobalY = pixelCluster->globalY();
      }else{
	clusterGlobalY = -999;
      }
      if( pixelCluster->isAvailable< float >( "globalZ" ) ){
	clusterGlobalZ = pixelCluster->globalZ();
      }else{
	clusterGlobalZ = -999;
      }

      //     clusterLocalX = pixelCluster->localX();
      //     clusterLocalY = pixelCluster->localY();
      
      clusterPhiModule = pixelCluster->auxdata< int >("phi_module");
      clusterEtaModule = pixelCluster->auxdata< int >("eta_module");
      clusterNPixelsX = pixelCluster->auxdataConst< int >( "sizePhi" );
      if( pixelCluster->isAvailable< int >( "sizeEta" ) ){
	clusterNPixelsY = pixelCluster->auxdataConst< int >( "sizeEta" );
      }else if( pixelCluster->isAvailable< int >( "sizeZ" ) ){
	clusterNPixelsY = pixelCluster->auxdataConst< int >( "sizeZ" );
      }else{
	clusterNPixelsY = -1;
      }
      clusterTruthBarcode = -999;
      clusterTruthPtcPt = -999;
      clusterTruthPtcPdgId = -999;
      clusterTrackPt = -999;
      
      int truthPtcBarcode = -1;
      
      if(isMC){
	if(pixelSiHitBarcode.size()>0)clusterSiHitBarcode =  pixelSiHitBarcode.at(0);
	if(pixelTruthBarcode.size()>0){
	  clusterTruthBarcode =  pixelTruthBarcode.at(0);
	  
	  // loop over the truth particles in the container
	  for( auto truthPtc_itr = truthPtcs->begin(); truthPtc_itr != truthPtcs->end(); truthPtc_itr++) {
	    if((*truthPtc_itr)->barcode() == clusterTruthBarcode){
	      clusterTruthPtcPt = (*truthPtc_itr)->pt()/1000.;
	      truthPtcBarcode = (*truthPtc_itr)->barcode();
	      clusterTruthPtcPdgId = (*truthPtc_itr)->pdgId();
	      break;
	    }else{
	      clusterTruthPtcPdgId = -999;
	      clusterTruthPtcPt = -999;
	    }
	  }
	  if( truthPtcBarcode > 0){
	    for( auto recoTrk_itr = recoTracks->begin(); recoTrk_itr != recoTracks->end(); recoTrk_itr++) {
	      
	      typedef ElementLink< xAOD::TruthParticleContainer > Link_t;
	      static const char* NAME = "truthParticleLink";
	      if( (*recoTrk_itr)->isAvailable< Link_t >( NAME ) ) {
		const Link_t& link = (*recoTrk_itr)->auxdata< Link_t >( NAME );
		if( link.isValid() ) {
		  xAOD::TruthParticle *truthParticle = (xAOD::TruthParticle*) *link;
		  if(truthParticle->barcode() == truthPtcBarcode){
		    
		    clusterTrackPt  =  (*recoTrk_itr)->pt()/1000.;
		    break;
		  }
		}
	      }
	    }
	  }
	  
	}
      }
	
      //rdo info associated to the cluster
      static SG::AuxElement::ConstAccessor< std::vector<float> >rdo_charge("rdo_charge");
      static SG::AuxElement::ConstAccessor< std::vector<int> >rdo_tot("rdo_tot");
      static SG::AuxElement::ConstAccessor< std::vector<int> >rdo_phi_pixel_index("rdo_phi_pixel_index");
      static SG::AuxElement::ConstAccessor< std::vector<int> >rdo_eta_pixel_index("rdo_eta_pixel_index");
      
      if(pixelCluster->isAvailable<std::vector<float>>("rdo_charge")){
	for(int ip=0; ip<(int)rdo_charge(*pixelCluster).size(); ip++){
	  clus_pixelIndex.push_back(ip);
	  clus_pixelCharge.push_back((int)(rdo_charge(*pixelCluster).at(ip)));
	  if(pixelCluster->isAvailable<std::vector<int>>("rdo_eta_pixel_index"))clus_pixelCol.push_back((int)(rdo_eta_pixel_index(*pixelCluster).at(ip)));
	  if(pixelCluster->isAvailable<std::vector<int>>("rdo_phi_pixel_index"))clus_pixelRow.push_back((int)(rdo_phi_pixel_index(*pixelCluster).at(ip)));
	  if((pixelCluster->isAvailable<std::vector<int>>("rdo_tot")) && (int)rdo_tot(*pixelCluster).size() > ip && rdo_tot(*pixelCluster).at(ip) !=0){
	    clus_pixelToT.push_back((int)(rdo_tot(*pixelCluster).at(ip)));
	  }else{
	    clus_pixelToT.push_back(-1);
	  }
	}
      }
   
      if(clusterToT > 0 && clusterToT < 999)m_clustertree->Fill();
    
    }

    nIntIBL += nIBL;
    nIntBL += nBL;
    nIntL1 += nL1;
    nIntL2 += nL2;
    nIntEC += nEC;
    //    std::cout << "Run " << m_runNumber << " Lumi Block " << lumiB << " PU " << m_eventPU << " IBL " << nIBL << " BL " << nBL << " L1 " << nL1 << " L2 " << nL2 << " EndCap " << nEC << std::endl;

  }

  // Split Track tuple
  
  if(doSplitTracks){

    // loop over the split tracks in the container
    for( auto recoSplitTrk_itr = recoSplitTracks->begin(); recoSplitTrk_itr != recoSplitTracks->end(); recoSplitTrk_itr++) {
      
      // Track selection
      if ( (*recoSplitTrk_itr)->pt()/1000. < minTrackPt) continue;
      
      //
      uint8_t numberOfSCTHits= 0;
      uint8_t numberOfTRTHits= 0;
      uint8_t numberOfPixelLayers= 0;
      uint8_t numberOfPixelHits= 0;
      uint8_t numberOfIBLHits= 0;
      uint8_t numberOfBLHits= 0;
      
      if(  ! (*recoSplitTrk_itr)->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits)) 
	Error("passTrackQualitySelection()", "Failed to retrieved summaryValue Pixel Hits" );
      if(  ! (*recoSplitTrk_itr)->summaryValue(numberOfSCTHits,xAOD::numberOfSCTHits))
	Error("passTrackQualitySelection()", "Failed to retrieved summaryValue SCT hits" );
      if(  ! (*recoSplitTrk_itr)->summaryValue(numberOfTRTHits,xAOD::numberOfTRTHits))
	Error("passTrackQualitySelection()", "Failed to retrieved summaryValue TRT hits" );
      
      (*recoSplitTrk_itr)->summaryValue(numberOfIBLHits,xAOD::numberOfInnermostPixelLayerHits);
      
      (*recoSplitTrk_itr)->summaryValue(numberOfBLHits,xAOD::numberOfNextToInnermostPixelLayerHits);
      
      (*recoSplitTrk_itr)->summaryValue(numberOfPixelLayers,xAOD::numberOfContribPixelLayers);
      
      if( numberOfPixelHits < minPixelHits || numberOfPixelHits+numberOfSCTHits < minSiHits || numberOfTRTHits < minTRTHits) continue;
      
      nPixelLayers = numberOfPixelLayers;
      nPixelHits = numberOfPixelHits;
      nSCTHits = numberOfSCTHits;
      nTRTHits = numberOfTRTHits;
      nIBLHits = numberOfIBLHits;
      nBLHits = numberOfBLHits;

      splitTrackPt  = (*recoSplitTrk_itr)->pt()/1000.;
      splitTrackEta = (*recoSplitTrk_itr)->eta();
      splitTrackPhi = (*recoSplitTrk_itr)->phi();
      splitTrackTheta = (*recoSplitTrk_itr)->theta();
      splitTrackD0  = (*recoSplitTrk_itr)->d0();
      splitTrackZ0  = (*recoSplitTrk_itr)->z0();
      splitTrackCharge = (*recoSplitTrk_itr)->charge();
      
      splitTrackD0Err = TMath::Sqrt((*recoSplitTrk_itr)->definingParametersCovMatrix()(0,0));
      splitTrackZ0Err = TMath::Sqrt((*recoSplitTrk_itr)->definingParametersCovMatrix()(1,1));
      splitTrackqOverPErr = TMath::Sqrt((*recoSplitTrk_itr)->definingParametersCovMatrix()(4,4));
     
      missDistRPhi = 999.;
      missDistZ = 999.;

      for( auto recoSplitTrk_itr2 = recoSplitTracks->begin(); recoSplitTrk_itr2 != recoSplitTracks->end(); recoSplitTrk_itr2++) {
      
	if(recoSplitTrk_itr != recoSplitTrk_itr2){
	  if(fabs(((*recoSplitTrk_itr)->pt()- (*recoSplitTrk_itr2)->pt())/ (*recoSplitTrk_itr)->pt()) < 0.10 && fabs(((*recoSplitTrk_itr)->theta()- (*recoSplitTrk_itr2)->theta())/ (*recoSplitTrk_itr)->theta()) < 0.10 && fabs(((*recoSplitTrk_itr)->phi()- (*recoSplitTrk_itr2)->phi())/ (*recoSplitTrk_itr)->phi()) < 0.10){

	    uint8_t numberOfPixelHits2= 0;
	    uint8_t numberOfIBLHits2= 0;
	    uint8_t numberOfBLHits2= 0;
	    uint8_t numberOfSCTHits2= 0;
	    (*recoSplitTrk_itr2)->summaryValue(numberOfPixelHits2,xAOD::numberOfPixelHits);
	    (*recoSplitTrk_itr2)->summaryValue(numberOfIBLHits2,xAOD::numberOfInnermostPixelLayerHits);
	    (*recoSplitTrk_itr2)->summaryValue(numberOfBLHits2,xAOD::numberOfNextToInnermostPixelLayerHits);
	    (*recoSplitTrk_itr2)->summaryValue(numberOfSCTHits2,xAOD::numberOfSCTHits);

	    nPixelHits2 = numberOfPixelHits2;
	    nSCTHits2 = numberOfSCTHits2;
	    nIBLHits2 = numberOfIBLHits2;
	    nBLHits2 = numberOfBLHits2;

	    if(numberOfPixelHits2 >= minPixelHits && numberOfPixelHits2+numberOfSCTHits2 >= minSiHits){
	      missDistRPhi = (*recoSplitTrk_itr)->d0()- (*recoSplitTrk_itr2)->d0();
	      missDistZ = (*recoSplitTrk_itr)->z0()- (*recoSplitTrk_itr2)->z0();
	    }
	  }
	}
      }

      //      std::cout << (*recoSplitTrk_itr)->pt()/1000. << " " << (*recoSplitTrk_itr)->eta() << " " <<  (*recoSplitTrk_itr)->phi() << " " <<  (*recoSplitTrk_itr)->theta() << (int)(*recoSplitTrk_itr)->charge() << " " << (*recoSplitTrk_itr)->d0() << " " << (*recoSplitTrk_itr)->z0() << " " << missDistRPhi << " " << missDistZ << std::endl;

      if(fabs(missDistRPhi) < 10.)m_splittrktree->Fill();
    }
  }
 
  /*
  // loop over the Fwd tracks in the container
  // auto type: xAOD::TrackParticleContainer::const_iterator
  for( auto recoFwdTrk_itr = recoFwdTracks->begin(); recoFwdTrk_itr != recoFwdTracks->end(); recoFwdTrk_itr++) {

    //    Float_t fwdTrackPt  = (*recoFwdTrk_itr)->pt()/1000.;
    //    Float_t fwdTrackEta = (*recoFwdTrk_itr)->eta();
    //    Float_t fwdTrackPhi = (*recoFwdTrk_itr)->phi();
    //    Float_t fwdTrackTheta = (*recoFwdTrk_itr)->theta();
    uint8_t numberOfPixelHits= 0;
    if((*recoFwdTrk_itr)->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits)) std::cout << "Fwd Track: " <<  numberOfPixelHits << std::endl;

  }
  */

  Int_t nPrint = 0;


  //Primary Vtx

  std::vector<float> pVtx;
  std::vector<float> dTrkPVtx;

  /*
  pVtx.push_back(0.);
  pVtx.push_back(0.);
  pVtx.push_back(0.);
  */
 
  /*
  pVtx.push_back(pVtxX);
  pVtx.push_back(pVtxY);
  pVtx.push_back(pVtxZ);
  */

  pVtx.push_back(-0.675);
  pVtx.push_back(-0.22);
  pVtx.push_back(0.);

  // Track tuple
  
  if(doTrackTree){
  // loop over the tracks in the container
  // auto type: xAOD::TrackParticleContainer::const_iterator
  for( auto recoTrk_itr = recoTracks->begin(); recoTrk_itr != recoTracks->end(); recoTrk_itr++) {


    // track selection

    //    std::cout << (*recoTrk_itr)->pt()/1000. << " " << fabs((*recoTrk_itr)->d0()) << " " << fabs(((*recoTrk_itr)->z0() + (*recoTrk_itr)->vz() - pVtxZ) * sin((*recoTrk_itr)->theta())) << std::endl;
											    
    if ( (*recoTrk_itr)->pt()/1000. < minTrackPt || ((*recoTrk_itr)->pt()/1000. > 500. && !isCosmic) || (fabs((*recoTrk_itr)->d0()) > maxD0 && !isCosmic) || (fabs(((*recoTrk_itr)->z0() + (*recoTrk_itr)->vz() - pVtxZ) * sin((*recoTrk_itr)->theta())) > maxDeltaZ && fabs(pVtxZ) < 500. && !isCosmic)) continue;

    //    std::cout << "Track Passed " << std::endl;

    //

    n3DHits = 0;

    uint8_t numberOfSCTHits= 0;
    uint8_t numberOfSCTHoles= 0;
    uint8_t numberOfTRTHits= 0;
    uint8_t numberOfPixelLayers= 0;
    uint8_t numberOfPixelHits= 0;
    uint8_t numberOfPixelHoles= 0;
    uint8_t numberOfPixelOutliers= 0;
    uint8_t numberOfIBLHits= 0;
    uint8_t numberOfIBLSplitHits= 0;
    uint8_t numberOfIBLSharedHits= 0;
    uint8_t numberOfBLHits= 0;
    uint8_t numberOfBLSplitHits= 0;
    uint8_t numberOfBLSharedHits= 0;

    uint8_t numberOfIBLExpectedHits= 0;
    uint8_t numberOfBLExpectedHits= 0;
    uint8_t numberOfSCTSharedHits= 0;

    if(  ! (*recoTrk_itr)->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits)) 
      Error("passTrackQualitySelection()", "Failed to retrieved summaryValue Pixel Hits" );
    if(  ! (*recoTrk_itr)->summaryValue(numberOfSCTHits,xAOD::numberOfSCTHits))
      Error("passTrackQualitySelection()", "Failed to retrieved summaryValue SCT hits" );
    if(  ! (*recoTrk_itr)->summaryValue(numberOfTRTHits,xAOD::numberOfTRTHits))
      Error("passTrackQualitySelection()", "Failed to retrieved summaryValue TRT hits" );

    (*recoTrk_itr)->summaryValue(numberOfIBLHits,xAOD::numberOfInnermostPixelLayerHits);
    (*recoTrk_itr)->summaryValue(numberOfIBLSplitHits,xAOD::numberOfInnermostPixelLayerSplitHits);
    (*recoTrk_itr)->summaryValue(numberOfIBLSharedHits,xAOD::numberOfInnermostPixelLayerSharedHits);

    (*recoTrk_itr)->summaryValue(numberOfBLHits,xAOD::numberOfNextToInnermostPixelLayerHits);
    (*recoTrk_itr)->summaryValue(numberOfBLSplitHits,xAOD::numberOfNextToInnermostPixelLayerSplitHits);
    (*recoTrk_itr)->summaryValue(numberOfBLSharedHits,xAOD::numberOfNextToInnermostPixelLayerSharedHits);

    (*recoTrk_itr)->summaryValue(numberOfPixelLayers,xAOD::numberOfContribPixelLayers);
    (*recoTrk_itr)->summaryValue(numberOfPixelHoles,xAOD::numberOfPixelHoles);
    (*recoTrk_itr)->summaryValue(numberOfPixelOutliers,xAOD::numberOfPixelOutliers);
    (*recoTrk_itr)->summaryValue(numberOfSCTHoles,xAOD::numberOfSCTHoles);

    (*recoTrk_itr)->summaryValue(numberOfIBLExpectedHits,xAOD::expectInnermostPixelLayerHit);
    (*recoTrk_itr)->summaryValue(numberOfBLExpectedHits,xAOD::expectNextToInnermostPixelLayerHit);
    (*recoTrk_itr)->summaryValue(numberOfSCTSharedHits,xAOD::numberOfSCTSharedHits);

    //    std::cout << numberOfPixelHits << " " << numberOfPixelHits+numberOfSCTHits << std::endl;

    if( numberOfPixelHits == 0 || numberOfPixelHits+numberOfSCTHits < 4) continue;

    TVector3 thisTrk((*recoTrk_itr)->p4().Px(),(*recoTrk_itr)->p4().Py(),(*recoTrk_itr)->p4().Pz());

    trackPt  = (*recoTrk_itr)->pt()/1000.;
    trackEta = (*recoTrk_itr)->eta();
    trackPhi = (*recoTrk_itr)->phi();
    trackTheta = (*recoTrk_itr)->theta();
    trackD0  = (*recoTrk_itr)->d0();
    trackZ0  = (*recoTrk_itr)->z0();
    trackCharge = (*recoTrk_itr)->charge();

    trackD0Err = TMath::Sqrt((*recoTrk_itr)->definingParametersCovMatrix()(0,0));
    trackZ0Err = TMath::Sqrt((*recoTrk_itr)->definingParametersCovMatrix()(1,1));
    trackqOverPErr = TMath::Sqrt((*recoTrk_itr)->definingParametersCovMatrix()(4,4));

    trackDeltaZSinTheta = ((*recoTrk_itr)->z0()+((*recoTrk_itr)->vz())-pVtxZ) * sin((*recoTrk_itr)->theta());

    if(pVtx[0]>-500.){
      if( (*recoTrk_itr)->isAvailable< float >( "IDTIDE1_unbiased_d0" )){
	trackD0PV = (*recoTrk_itr)->auxdata< float >( "IDTIDE1_unbiased_d0");
	trackZ0PV = (*recoTrk_itr)->auxdata< float >( "IDTIDE1_unbiased_z0");
      }else if((*recoTrk_itr)->vx()==0.){
	dTrkPVtx.clear();
	dTrkPVtx = trkD0ToVtx(*recoTrk_itr, pVtx);
	
	trackD0PV  = dTrkPVtx[0];
	trackZ0PV  = dTrkPVtx[1]; //(*recoTrk_itr)->z0() - pVtxZ;
	
	if(trackPt>1.5 && nPrint<10 && m_eventCounter < 10){
	  
	  std::cout << "Track: d0 " << (*recoTrk_itr)->d0() << "; z0 " << (*recoTrk_itr)->z0() << "; Ref (" << (*recoTrk_itr)->vx() << ", " << (*recoTrk_itr)->vy() << ", " << (*recoTrk_itr)->vz() << ") " << " PVtx (" << pVtxX << " " << pVtx[0] << ", " << pVtxY << " " << pVtx[1] << ", " << pVtxZ << " " << pVtx[2] << ")" << "; d0Vtx " << dTrkPVtx[0] << "; z0Vtx " << dTrkPVtx[1] << " " << (*recoTrk_itr)->z0() - pVtxZ << "; Q " << (*recoTrk_itr)->charge() << std::endl;
	  nPrint++;
	}
      }
    }
  

    if(hasRefit){

      trackRefitPt  = (*recoTrk_itr)->auxdata< float > ("refitPt");
      trackRefitD0  = (*recoTrk_itr)->auxdata< float > ("refitD0");
      trackRefitD0Err  = (*recoTrk_itr)->auxdata< float > ("refitD0Err");
      trackRefitZ0  = (*recoTrk_itr)->auxdata< float > ("refitZ0");
      trackRefitZ0Err  = (*recoTrk_itr)->auxdata< float > ("refitZ0Err");
      trackRefitZ0SinTheta  = (*recoTrk_itr)->auxdata< float > ("refitZ0SinTheta");
      trackRefitZ0SinThetaErr  = (*recoTrk_itr)->auxdata< float > ("refitZ0SinThetaErr");
      trackRefitNPixelHits  = (*recoTrk_itr)->auxdata< int > ("refitNbOfPixelHits");
      trackRefitNPixelLayers  = (*recoTrk_itr)->auxdata< int > ("refitNbOfPixelLayers");
    }else{
      trackRefitPt  = -999.;
      trackRefitD0  = -999.;
      trackRefitD0Err  = -999.;
      trackRefitZ0  = -999;
      trackRefitZ0Err  = -999;
      trackRefitZ0SinTheta  = -999;
      trackRefitZ0SinThetaErr  = -999;
      trackRefitNPixelHits  = 0;
      trackRefitNPixelLayers  = 0;
    }
    
    resiXIBL = 999;
    resiYIBL = 999;
    pullXIBL = 999;
    pullYIBL = 999;
    resibXIBL = 999;
    resibYIBL = 999;
    pullbXIBL = 999;
    pullbYIBL = 999;

    mcFlag = 0;
    if(isMC) mcFlag = 1;
   
    lumiB = eventInfo->lumiBlock();
    bcid = eventInfo->bcid();
    
    nPixelLayers = numberOfPixelLayers;
    nPixelHits = numberOfPixelHits;
    nPixelHoles = numberOfPixelHoles;
    nPixelOutliers = numberOfPixelOutliers;
    nSCTHits = numberOfSCTHits;
    nSCTHoles = numberOfSCTHoles;
    nTRTHits = numberOfTRTHits;
    nIBLHits = numberOfIBLHits;
    nBLHits = numberOfBLHits;
    nIBLSplitHits = numberOfIBLSplitHits;
    nBLSplitHits = numberOfBLSplitHits;
    nIBLSharedHits = numberOfIBLSharedHits;
    nBLSharedHits = numberOfBLSharedHits;

    nIBLExpectedHits = numberOfIBLExpectedHits;
    nBLExpectedHits = numberOfBLExpectedHits;
    nSCTSharedHits = numberOfSCTSharedHits;

    genVtxX = -1;
    genVtxY = -1;
    genVtxR = -1;
    genVtxZ = -1;
    parentFlav = -1;

    trueD0 = 999.;
    trueZ0 = 999.;
    truePhi = 999;
    trueTheta = 999;
    trueqOverP = 999;

    hitLocalXIBL = -999;
    hitLocalYIBL = -999;
    hitLocalXG4IBL = -999;
    hitLocalYG4IBL = -999;

    hitSplitIBL =0;
    hitSplitBL =0;
    hitSplitL2 =0;
    hitSplitL3 =0;

    nL0Hits =0;
    nL1Hits =0;
    nL2Hits =0;
    nL3Hits =0;
    nECHits =0;

    truebarcode = -1;
    truepdgid = -1;

    trackMatchProb = -1;

    float minJetdR = 2.0;
    float minJetdRPt = -999;
    int minJetFlavour = -999;
    int minJetTruthLabel = -999;

    if(doJets && jets !=0){
      for( auto jet_itr = jets->begin(); jet_itr != jets->end(); jet_itr++) {
	TVector3 thisJet((*jet_itr)->px(),(*jet_itr)->py(),(*jet_itr)->pz());
	
	float deltaR = thisTrk.DeltaR(thisJet);
	if(deltaR<minJetdR){
	  minJetdRPt = (*jet_itr)->pt()/1000.;
	  minJetdR = deltaR;
	}
	if(isMC){
	  if((*jet_itr)->isAvailable< int >( "PartonTruthLabelID" )){
	    minJetFlavour = (*jet_itr)->getAttribute<int>("PartonTruthLabelID");
	  }
	  if((*jet_itr)->isAvailable< int >( "HadronConeExclTruthLabelID" )){

	    minJetTruthLabel = (*jet_itr)->getAttribute<int>("HadronConeExclTruthLabelID");
	  }
	}
      }
      trackJetdR = minJetdR;
      trackJetPt = minJetdRPt;
      trackJetFlavour = minJetFlavour;
      trackJetHadronConeExclTruthLabel = minJetTruthLabel;
    }else{
      trackJetdR =  999;
      trackJetPt = -999;
    }

    if(isMC){
      typedef ElementLink< xAOD::TruthParticleContainer > Link_t;
      static const char* NAME = "truthParticleLink";
      if( (*recoTrk_itr)->isAvailable< Link_t >( NAME ) ) {
	const Link_t& link = (*recoTrk_itr)->auxdata< Link_t >( NAME );
	if( link.isValid() ) {
	  xAOD::TruthParticle *truthParticle = (xAOD::TruthParticle*) *link;
	  if(  truthParticle->isAvailable< float >( "d0" )){
	    trueD0 = truthParticle->auxdataConst< float >( "d0" );
	  }else{
	    trueD0 = -999;
	  }
	  if(  truthParticle->isAvailable< float >( "z0" )){
	    trueZ0 = truthParticle->auxdataConst< float >( "z0" );
	  }else{
	    trueZ0 = -999;
	  }
	  if(  truthParticle->isAvailable< float >( "phi" )){
	    truePhi = truthParticle->auxdataConst< float >( "phi" );
	  }else{
	    truePhi = -999;
	  }
	  if(  truthParticle->isAvailable< float >( "theta" )){
	    trueTheta = truthParticle->auxdataConst< float >( "theta" );
	  }else{
	    trueTheta = -999;
	  }
	  if(  truthParticle->isAvailable< float >( "qOverP" )){
	    trueqOverP = truthParticle->auxdataConst< float >( "qOverP" );
	  }else{
	    trueqOverP = -999;
	  }

	  if(  truthParticle->isAvailable< float >( "truthMatchProbability" ) ){
	    truthMatchProb = truthParticle->auxdataConst< float >( "truthMatchProbability" );
	  }else{
	    truthMatchProb = -1;
	  }
	 
	  trackMatchProb = (*recoTrk_itr)->auxdata< float >( "truthMatchProbability" );

	  truebarcode = truthParticle->barcode();
	  truepdgid = truthParticle->pdgId();

	  if(truthParticle->hasProdVtx()){
	    if(  truthParticle->prodVtx()->isAvailable< float >("x") &&
		 truthParticle->prodVtx()->isAvailable< float >("y") &&
		 truthParticle->prodVtx()->isAvailable< float >("z") ){

	      genVtxR = sqrt( truthParticle->prodVtx()->x()* truthParticle->prodVtx()->x() +  truthParticle->prodVtx()->y()* truthParticle->prodVtx()->y());
	      genVtxX =  truthParticle->prodVtx()->x();
	      genVtxY =  truthParticle->prodVtx()->y();
	      genVtxZ =  truthParticle->prodVtx()->z();
	    }else{
	      genVtxR = 0;
	      genVtxZ = 0;
	    }
	  }else{
	    genVtxR = 0;
	    genVtxZ = 0;
	  }
	  parentFlav = 1;
	  /*
	  for(int ip=0;ip<(int)truthParticle->nParents();ip++){
	    if((abs(truthParticle->parent(ip)->pdgId()) > 300 && abs(truthParticle->parent(ip)->pdgId()) < 400) || (abs(truthParticle->parent(ip)->pdgId()) > 3000 && abs(truthParticle->parent(ip)->pdgId()) < 4000)) parentFlav = 3;
	    if((abs(truthParticle->parent(ip)->pdgId()) > 400 && abs(truthParticle->parent(ip)->pdgId()) < 500) || (abs(truthParticle->parent(ip)->pdgId()) > 4000 && abs(truthParticle->parent(ip)->pdgId()) < 5000)) parentFlav = 4;
	    if((abs(truthParticle->parent(ip)->pdgId()) > 500 && abs(truthParticle->parent(ip)->pdgId()) < 600) || (abs(truthParticle->parent(ip)->pdgId()) > 5000 && abs(truthParticle->parent(ip)->pdgId()) < 6000)) parentFlav = 5;
	  }
	  */
	}
      }
    }

    pixeldEdx = (*recoTrk_itr)->auxdata< float >("pixeldEdx");
    nPixeldEdx = (*recoTrk_itr)->numberOfUsedHitsdEdx();

    if(hasPixelVector){
      pixelLayer = (*recoTrk_itr)->auxdata< std::vector<int> >("pixelVectorLayer");
      pixelIsSplit = (*recoTrk_itr)->auxdata< std::vector<int> >("pixelVectorSplit");

      pixelLocalX = (*recoTrk_itr)->auxdata< std::vector<float> >("pixelVectorLocalX");
      pixelLocalErrorX = (*recoTrk_itr)->auxdata< std::vector<float> >("pixelVectorLocalErrX");
      pixelLocalY = (*recoTrk_itr)->auxdata< std::vector<float> >("pixelVectorLocalY");
      pixelLocalErrorY = (*recoTrk_itr)->auxdata< std::vector<float> >("pixelVectorLocalErrY");
      trkHitLocalX = (*recoTrk_itr)->auxdata< std::vector<float> >("hitVectorLocalX");
      //      trkHitLocalErrorX = (*recoTrk_itr)->auxdata< std::vector<float> >("hitVectorLocalErrX");
      trkHitLocalY = (*recoTrk_itr)->auxdata< std::vector<float> >("hitVectorLocalY");
      //      trkHitLocalErrorY = (*recoTrk_itr)->auxdata< std::vector<float> >("hitVectorLocalErrY");
    }
	
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
    //    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;

    hitLayer.clear();
    hitIsEndCap.clear();
    hitIsHole.clear();
    hitIsOutlier.clear();
    hitIsIBL.clear();
    hitIs3D.clear();
    hitCharge.clear();
    hitToT.clear();
    hitLVL1A.clear();
    hitNPixel.clear();
    hitNPixelX.clear();
    hitNPixelY.clear();
    hitIsSplit.clear();
    hitSplitProb1.clear();
    hitSplitProb2.clear();
    hitSplitProb3.clear();
    hitEtaModule.clear();
    hitPhiModule.clear();
    hitLocalX.clear();
    hitLocalY.clear();
    clustLocalX.clear();
    clustLocalY.clear();
    g4LocalX.clear();
    g4LocalY.clear();
    g4EntryLocalX.clear();
    g4EntryLocalY.clear();
    g4ExitLocalX.clear();
    g4ExitLocalY.clear();
    g4Barcode.clear();
    g4PdgId.clear();
    g4EnergyDeposit.clear();
    unbiasedResidualX.clear();
    unbiasedResidualY.clear();
    biasedResidualX.clear();
    biasedResidualY.clear();
    unbiasedPullX.clear();
    unbiasedPullY.clear();
    biasedPullX.clear();
    biasedPullY.clear();

    unbiasedResidualRefitX.clear();
    unbiasedResidualRefitY.clear();
    unbiasedResidualRefitXErr.clear();
    unbiasedResidualRefitYErr.clear();
    biasedResidualRefitX.clear();
    biasedResidualRefitY.clear();
    biasedResidualRefitXErr.clear();
    biasedResidualRefitYErr.clear();
    trkRefitLocalX.clear();
    trkRefitLocalY.clear();

    hitVBias.clear();
    hitVDep.clear();
    hitTemp.clear();
    hitLorentzShift.clear();
    hitTanLorentzAngle.clear();

    hitGlobalX.clear();
    hitGlobalY.clear();
    hitGlobalZ.clear();

    trkLocalX.clear();
    trkLocalY.clear();
    hitLocalErrorX.clear();
    hitLocalErrorY.clear();
    trkLocalErrorX.clear();
    trkLocalErrorY.clear();
    minTrkG4DistX.clear();
    minTrkG4DistY.clear();
    minTrkDistX.clear();
    minTrkDistY.clear();

    trkDens10.clear();
    trkDens25.clear();
    trkDens50.clear();
    trkDensPitch2.clear();
    trkDensPitch5.clear();
    trkDensPitch10.clear();
    hitDens10.clear();
    hitDens25.clear();
    hitDens50.clear();

    hit3DClusterDX.clear();
    hit3DClusterDY.clear();

    trkPhiOnSurface.clear();
    trkThetaOnSurface.clear();

    clus_nContributingPtcs.clear();
    clus_nContributingPU.clear();
    clus_contributingPtcsBarcode.clear();
    clus_contributingPtcsPdgId.clear();
    clus_contributingPtcsPt.clear();
    clus_contributingPtcsE.clear();

    IBL_NN_phiBS.clear();
    IBL_NN_thetaBS.clear();  
    IBL_NN_matrixOfCharge.clear();
    IBL_NN_vectorOfPitchesY.clear();
    IBL_NN_localPhiPixelIndexWeightedPosition.clear();
    IBL_NN_localEtaPixelIndexWeightedPosition.clear();

    BL_NN_phiBS.clear();
    BL_NN_thetaBS.clear();  
    BL_NN_matrixOfCharge.clear();
    BL_NN_vectorOfPitchesY.clear();
    BL_NN_localPhiPixelIndexWeightedPosition.clear();
    BL_NN_localEtaPixelIndexWeightedPosition.clear();

    clus_pixelIBLIndex.clear();
    clus_pixelIBLRow.clear();
    clus_pixelIBLCol.clear();
    clus_pixelIBLCharge.clear();
    clus_pixelIBLToT.clear();

    clus_pixelBLIndex.clear();
    clus_pixelBLRow.clear();
    clus_pixelBLCol.clear();
    clus_pixelBLCharge.clear();
    clus_pixelBLToT.clear();

    clus_pixelL2Index.clear();
    clus_pixelL2Row.clear();
    clus_pixelL2Col.clear();
    clus_pixelL2Charge.clear();
    clus_pixelL2ToT.clear();

    clus_pixelL3Index.clear();
    clus_pixelL3Row.clear();
    clus_pixelL3Col.clear();
    clus_pixelL3Charge.clear();
    clus_pixelL3ToT.clear();

    TVector2 theTrkExtr(-999,-999);

    static const char* measurementNames = "msosLink";   //Note the prefix could change Rel21
    //    static const char* measurementNames = "IDTIDEmsosLink";   //Note the prefix could change
    //    static const char* measurementNames = "IDDET1_msosLink";   //Note the prefix could change

    if( ! (*recoTrk_itr)->isAvailable< MeasurementsOnTrack >( measurementNames ) ) {
      Info("execute()",  "no MSOS Available");
      continue;
    }

    TVector3 hitIBL;
    TVector3 hitBLayer;
    TVector3 hitLayer1;
    TVector3 hitLayer2;
    
    Bool_t hasIBL = false;
    Bool_t hasBLayer = false;
    Bool_t hasLayer1 = false;
    Bool_t hasLayer2 = false;

    // Get the MSOS's
    const MeasurementsOnTrack& measurementsOnTrack = (*recoTrk_itr)->auxdata< MeasurementsOnTrack >( measurementNames );

    // Loop over track MeasurementStateOnSurface's
  
    for( auto msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter)
    {  
      //Check if the element link is valid
      if( ! (*msos_iter).isValid() ) {
        continue;
      }
      
      const xAOD::TrackStateValidation* msos = *(*msos_iter); 

      /*
      if( ! msos->isAvailable< int >( "detType" ) ) {
        continue;
      }
      */

      // Holes

    
      if(msos->detType() == 1 &&  msos->type() == TrackStateOnSurface::Hole && storeHoles) {
	
	GetLayerEtaPhiFromId((uint64_t)msos->detElementId());

	hitLayer.push_back(holeLayer);
	hitIsEndCap.push_back(holeBarrelEC);
	hitIsSplit.push_back(0);
	hitIsHole.push_back(1);
	hitIsOutlier.push_back(0);

	trkPhiOnSurface.push_back(msos->auxdata< float > ("localPhi"));
	trkThetaOnSurface.push_back(msos->auxdata< float > ("localTheta"));    

	if(holeLayer == 0 && holeBarrelEC == 0){
	  hitIsIBL.push_back(1);
	  if(holeEtaModule <=5 && holeEtaModule>=-6){
	      hitIs3D.push_back(0);
	    }else{
	      hitIs3D.push_back(1);
	  }
	}else{
	  hitIsIBL.push_back(0);
	  hitIs3D.push_back(0);
	} 
	hitGlobalX.push_back(0);
	hitGlobalY.push_back(0);
	hitGlobalZ.push_back(0);
	hitLocalX.push_back(msos->localX());
	hitLocalY.push_back(msos->localY());
	clustLocalX.push_back(msos->localX());
	clustLocalY.push_back(msos->localY());
	hitCharge.push_back(0);
	hitToT.push_back(0);
	hitLVL1A.push_back(-999);
	hitNPixel.push_back(0);
	hitNPixelX.push_back(0);
	hitNPixelY.push_back(0);
	hitEtaModule.push_back(holeEtaModule);
	hitPhiModule.push_back(holePhiModule);

	hitIsSplit.push_back(0);
	hitSplitProb1.push_back(0);
	hitSplitProb2.push_back(0);
	hitSplitProb3.push_back(0);
	if(isMC){
	  g4LocalX.push_back(-999);
	  g4LocalY.push_back(-999);
	  g4EntryLocalX.push_back(-999);
	  g4EntryLocalY.push_back(-999);
	  g4ExitLocalX.push_back(-999);
	  g4ExitLocalY.push_back(-999);
	  g4Barcode.push_back(-1);
	  g4PdgId.push_back(-999);
	  g4EnergyDeposit.push_back(0);
	  minTrkG4DistX.push_back(-999);
	  minTrkG4DistY.push_back(-999);
	}
	unbiasedResidualX.push_back(-999);
	unbiasedResidualY.push_back(-999);
	biasedResidualX.push_back(-999);
	biasedResidualY.push_back(-999);
	unbiasedPullX.push_back(-999);
	unbiasedPullY.push_back(-999);
	biasedPullX.push_back(-999);
	biasedPullY.push_back(-999);
	if(hasRefit){
	  unbiasedResidualRefitX.push_back(-999);
	  unbiasedResidualRefitY.push_back(-999);
	  unbiasedResidualRefitXErr.push_back(-999);
	  unbiasedResidualRefitYErr.push_back(-999);
	  biasedResidualRefitX.push_back(-999);
	  biasedResidualRefitY.push_back(-999);
	  biasedResidualRefitXErr.push_back(-999);
	  biasedResidualRefitYErr.push_back(-999);
	  trkRefitLocalX.push_back(-999);
	  trkRefitLocalY.push_back(-999);
	}
	hitVBias.push_back(-999);
	hitVDep.push_back(-999);
	hitTemp.push_back(-999);
	hitLorentzShift.push_back(-999);
	hitTanLorentzAngle.push_back(-999);
	trkLocalX.push_back(-999);
	trkLocalY.push_back(-999);
	hitLocalErrorX.push_back(-999);
	hitLocalErrorY.push_back(-999);
	trkLocalErrorX.push_back(-999);
	trkLocalErrorY.push_back(-999);
	minTrkDistX.push_back(-999);
	minTrkDistY.push_back(-999);
      }

      if(msos->detType() == 1 &&  msos->type() == TrackStateOnSurface::Outlier && storeOutliers) {
	
	GetLayerEtaPhiFromId((uint64_t)msos->detElementId());

	hitLayer.push_back(holeLayer);
	hitIsEndCap.push_back(holeBarrelEC);
	hitIsSplit.push_back(0);
	hitSplitProb1.push_back(0);
	hitSplitProb2.push_back(0);
	hitSplitProb3.push_back(0);
	hitIsHole.push_back(0);
	hitIsOutlier.push_back(1);

	trkPhiOnSurface.push_back(msos->auxdata< float > ("localPhi"));
	trkThetaOnSurface.push_back(msos->auxdata< float > ("localTheta"));    

	if(holeLayer == 0 && holeBarrelEC == 0){
	  hitIsIBL.push_back(1);
	  if(holeEtaModule <=5 && holeEtaModule>=-6){
	      hitIs3D.push_back(0);
	    }else{
	      hitIs3D.push_back(1);
	    }
	}else{
	  hitIsIBL.push_back(0);
	  hitIs3D.push_back(0);
	} 

	const xAOD::TrackMeasurementValidation* pixelCluster =  *(msos->trackMeasurementValidationLink());        

	trkPhiOnSurface.push_back(msos->auxdata< float > ("localPhi"));
	trkThetaOnSurface.push_back(msos->auxdata< float > ("localTheta"));    

	hitGlobalX.push_back(pixelCluster->auxdata< float >("globalX"));
	hitGlobalY.push_back(pixelCluster->auxdata< float >("globalY"));
	hitGlobalZ.push_back(pixelCluster->auxdata< float >("globalZ"));
	hitLocalX.push_back(pixelCluster->localX());
	hitLocalY.push_back(pixelCluster->localY());
	clustLocalX.push_back(pixelCluster->localX());
	clustLocalY.push_back(pixelCluster->localY());
	hitCharge.push_back(pixelCluster->auxdata< float >("charge"));
	hitToT.push_back(pixelCluster->auxdata< int >("ToT"));
	hitLVL1A.push_back(pixelCluster->auxdata< int >("LVL1A"));
	hitNPixel.push_back(pixelCluster->auxdataConst< int >( "nRDO" ));
	hitNPixelX.push_back(pixelCluster->auxdataConst< int >( "sizePhi" ));
	hitNPixelY.push_back(pixelCluster->auxdataConst< int >( "sizeZ" ));
	hitEtaModule.push_back(pixelCluster->auxdata< int >("eta_module"));
	hitPhiModule.push_back(pixelCluster->auxdata< int >("phi_module"));
	if(isMC){
	  g4LocalX.push_back(-999);
	  g4LocalY.push_back(-999);
	  g4EntryLocalX.push_back(-999);
	  g4EntryLocalY.push_back(-999);
	  g4ExitLocalX.push_back(-999);
	  g4ExitLocalY.push_back(-999);
	  g4Barcode.push_back(-1);
	  g4PdgId.push_back(-999);
	  g4EnergyDeposit.push_back(0);
	  minTrkG4DistX.push_back(-999);
	  minTrkG4DistY.push_back(-999);
	}

	float resiX  = msos->auxdata< float > ("unbiasedResidualX");
	float resiY  = msos->auxdata< float > ("unbiasedResidualY");
	float pullX  = msos->auxdata< float > ("unbiasedPullX");
	float pullY  = msos->auxdata< float > ("unbiasedPullY");
	
	float resibX = msos->auxdata< float > ("biasedResidualX");
	float resibY = msos->auxdata< float > ("biasedResidualY");
	float pullbX = msos->auxdata< float > ("biasedPullX");
	float pullbY = msos->auxdata< float > ("biasedPullY");
	
	theTrkExtr.Set(pixelCluster->localX()-resiX,pixelCluster->localY()-resiY);
	hitLocalErrorX.push_back(sqrt((pow(resiX/pullX,2) + pow(resibX/pullbX,2))/2.));
	hitLocalErrorY.push_back(sqrt((pow(resiY/pullY,2) + pow(resibY/pullbY,2))/2.));	  
	trkLocalErrorX.push_back(sqrt((pow(resiX/pullX,2) - pow(resibX/pullbX,2))/2.));
	trkLocalErrorY.push_back(sqrt((pow(resiY/pullY,2) - pow(resibY/pullbY,2))/2.));	  
	trkLocalX.push_back(pixelCluster->localX()-resiX);
	trkLocalY.push_back(pixelCluster->localY()-resiY);
	  
	unbiasedResidualX.push_back(resiX);
	unbiasedResidualY.push_back(resiY);
	unbiasedPullX.push_back(pullX);
	unbiasedPullY.push_back(pullY);
	biasedResidualX.push_back(resibX);
	biasedResidualY.push_back(resibY);
	biasedPullX.push_back(pullbX);
	biasedPullY.push_back(pullbY);

	if(hasRefit){
	  unbiasedResidualRefitX.push_back(-999);
	  unbiasedResidualRefitY.push_back(-999);
	  unbiasedResidualRefitXErr.push_back(-999);
	  unbiasedResidualRefitYErr.push_back(-999);
	  biasedResidualRefitX.push_back(-999);
	  biasedResidualRefitY.push_back(-999);
	  biasedResidualRefitXErr.push_back(-999);
	  biasedResidualRefitYErr.push_back(-999);
	  trkRefitLocalX.push_back(-999);
	  trkRefitLocalY.push_back(-999);
	}
	hitVBias.push_back(-999);
	hitVDep.push_back(-999);
	hitTemp.push_back(-999);
	hitLorentzShift.push_back(-999);
	hitTanLorentzAngle.push_back(-999);
	trkLocalX.push_back(-999);
	trkLocalY.push_back(-999);
	hitLocalErrorX.push_back(-999);
	hitLocalErrorY.push_back(-999);
	trkLocalErrorX.push_back(-999);
	trkLocalErrorY.push_back(-999);
	minTrkDistX.push_back(-999);
	minTrkDistY.push_back(-999);
      }

 
      if( msos->detType() != TrackState::Pixel  ||  msos->type() != TrackStateOnSurface::Measurement )

        continue;
    
      //Associated pixel cluster

      //Get pixel cluster
      if(  msos->trackMeasurementValidationLink().isValid() && *(msos->trackMeasurementValidationLink()) ){
        const xAOD::TrackMeasurementValidation* pixelCluster =  *(msos->trackMeasurementValidationLink());        

	int bec = pixelCluster->auxdata< int>("bec");
	int layer = pixelCluster->auxdata< int>("layer");
	int phiModule = pixelCluster->auxdata< int >("phi_module");
	int etaModule = pixelCluster->auxdata< int >("eta_module");
	static SG::AuxElement::ConstAccessor< std::vector<int> >  truth_barcode("truth_barcode");

	//rdo info associated to the cluster
	static SG::AuxElement::ConstAccessor< std::vector<float> >rdo_charge("rdo_charge");
      	static SG::AuxElement::ConstAccessor< std::vector<int> >rdo_tot("rdo_tot");
	static SG::AuxElement::ConstAccessor< std::vector<int> >rdo_phi_pixel_index("rdo_phi_pixel_index");
	static SG::AuxElement::ConstAccessor< std::vector<int> >rdo_eta_pixel_index("rdo_eta_pixel_index");

	//NN info associated to the cluster
	static SG::AuxElement::ConstAccessor< std::vector<float> >  NN_matrixOfCharge("NN_matrixOfCharge");
	static SG::AuxElement::ConstAccessor< std::vector<float> >  NN_matrixOfToT("NN_matrixOfToT");
	static SG::AuxElement::ConstAccessor< std::vector<int> >  NN_barcode("NN_barcode");
	static SG::AuxElement::ConstAccessor< std::vector<float> >  NN_vectorOfPitchesY("NN_vectorOfPitchesY");
	static SG::AuxElement::ConstAccessor<float>  NN_phiBS("NN_phiBS");
	static SG::AuxElement::ConstAccessor<float>  NN_thetaBS("NN_thetaBS");
	static SG::AuxElement::ConstAccessor<float>  NN_localPhiPixelIndexWeightedPosition("NN_localPhiPixelIndexWeightedPosition");
	static SG::AuxElement::ConstAccessor<float>  NN_localEtaPixelIndexWeightedPosition("NN_localEtaPixelIndexWeightedPosition");
    
	if (bec!=0)nECHits++;

	if(bec==0){
	  if(layer==0){
	    nL0Hits++;
	    
	    if(pixelCluster->auxdataConst< int >( "isSplit" ) !=0) hitSplitIBL++;
	    
	    if(pixelCluster->isAvailable<std::vector<float>>("rdo_charge")){
	      
	      for(int ip=0; ip<(int)rdo_charge(*pixelCluster).size(); ip++){
		clus_pixelIBLCharge.push_back((int)(rdo_charge(*pixelCluster).at(ip)));
		// Col -> eta, Row -> phi
		if(pixelCluster->isAvailable<std::vector<int>>("rdo_phi_pixel_index"))clus_pixelIBLRow.push_back((int)(rdo_phi_pixel_index(*pixelCluster).at(ip)));
		if(pixelCluster->isAvailable<std::vector<int>>("rdo_eta_pixel_index"))clus_pixelIBLCol.push_back((int)(rdo_eta_pixel_index(*pixelCluster).at(ip)));
		clus_pixelIBLIndex.push_back(ip);
		if(pixelCluster->isAvailable<std::vector<int>>("rdo_tot") && (int)rdo_tot(*pixelCluster).size() > ip && rdo_tot(*pixelCluster).at(ip) !=0){
		  clus_pixelIBLToT.push_back((int)(rdo_tot(*pixelCluster).at(ip)));
		}else{
		  clus_pixelIBLToT.push_back(-1);
		}
	      }
	      
	    }else if(hasPixelNN){
	      
	      for(int ip=0; ip<(int)NN_matrixOfCharge(*pixelCluster ).size(); ip++){
		if(NN_matrixOfCharge(*pixelCluster).at(ip) !=0){
		  
		  clus_pixelIBLCharge.push_back((int)NN_matrixOfCharge(*pixelCluster).at(ip));
		  clus_pixelIBLIndex.push_back(ip);
		  if(hasPixelToT&&(int)NN_matrixOfToT(*pixelCluster).size()>ip&&NN_matrixOfToT(*pixelCluster).at(ip) !=0){
		    clus_pixelIBLToT.push_back((int)NN_matrixOfToT(*pixelCluster).at(ip));
		  }
		}
	      }
	    }
	    
	    if(hasPixelNN && storePixelNN){

	      for(int ip=0; ip<(int)NN_matrixOfCharge(*pixelCluster ).size(); ip++){
		IBL_NN_matrixOfCharge.push_back((float)NN_matrixOfCharge(*pixelCluster).at(ip));
	      }	    
	      for(int ip=0; ip<(int)NN_vectorOfPitchesY(*pixelCluster ).size(); ip++){
		IBL_NN_vectorOfPitchesY.push_back((float)NN_vectorOfPitchesY(*pixelCluster).at(ip));
	      }
	      IBL_NN_phiBS.push_back((float)NN_phiBS(*pixelCluster));
	      IBL_NN_thetaBS.push_back((float)NN_thetaBS(*pixelCluster));
	      IBL_NN_localPhiPixelIndexWeightedPosition.push_back((float)NN_localPhiPixelIndexWeightedPosition(*pixelCluster));
	      IBL_NN_localEtaPixelIndexWeightedPosition.push_back((float)NN_localEtaPixelIndexWeightedPosition(*pixelCluster));

	    }
	    
	  }else if ( layer==1 ){
	    nL1Hits++;
	    if(pixelCluster->auxdataConst< int >( "isSplit" ) !=0) hitSplitBL++;
	    
	    if(pixelCluster->isAvailable<std::vector<float>>("rdo_charge")){
	      
	      for(int ip=0; ip<(int)rdo_charge(*pixelCluster).size(); ip++){
		if(rdo_charge(*pixelCluster).at(ip) !=0){
		  clus_pixelBLCharge.push_back((int)(rdo_charge(*pixelCluster).at(ip)));
		  clus_pixelBLRow.push_back((int)(rdo_phi_pixel_index(*pixelCluster).at(ip)));
		  clus_pixelBLCol.push_back((int)(rdo_eta_pixel_index(*pixelCluster).at(ip)));
		  clus_pixelBLIndex.push_back(ip);
		  if(pixelCluster->isAvailable<std::vector<int>>("rdo_tot")&& (int)rdo_tot(*pixelCluster).size() > ip  && rdo_tot(*pixelCluster).at(ip) !=0){
		    clus_pixelBLToT.push_back((int)(rdo_tot(*pixelCluster).at(ip)));
		  }else{
		    clus_pixelBLToT.push_back(-1);
		  }
		}
	      }
	    }else if(hasPixelNN){
	      
	      for(int ip=0; ip<(int)NN_matrixOfCharge(*pixelCluster ).size(); ip++){
		if(NN_matrixOfCharge(*pixelCluster).at(ip) !=0){
		  
		  clus_pixelBLCharge.push_back((int)NN_matrixOfCharge(*pixelCluster).at(ip));
		  clus_pixelBLIndex.push_back(ip);
		  if(hasPixelToT&&(int)NN_matrixOfToT(*pixelCluster).size()>ip&&NN_matrixOfToT(*pixelCluster).at(ip) !=0){
		    clus_pixelBLToT.push_back((int)NN_matrixOfToT(*pixelCluster).at(ip));
		  }
		}
	      }
	    }
	    if(hasPixelNN && storePixelNN){

	      for(int ip=0; ip<(int)NN_matrixOfCharge(*pixelCluster ).size(); ip++){
		BL_NN_matrixOfCharge.push_back((float)NN_matrixOfCharge(*pixelCluster).at(ip));
	      }	    
	      for(int ip=0; ip<(int)NN_vectorOfPitchesY(*pixelCluster ).size(); ip++){
		BL_NN_vectorOfPitchesY.push_back((float)NN_vectorOfPitchesY(*pixelCluster).at(ip));
	      }
	      BL_NN_phiBS.push_back((float)NN_phiBS(*pixelCluster));
	      BL_NN_thetaBS.push_back((float)NN_thetaBS(*pixelCluster));
	      BL_NN_localPhiPixelIndexWeightedPosition.push_back((float)NN_localPhiPixelIndexWeightedPosition(*pixelCluster));
	      BL_NN_localEtaPixelIndexWeightedPosition.push_back((float)NN_localEtaPixelIndexWeightedPosition(*pixelCluster));

	    }

	  }else if ( layer==2 ){
	    
	    nL2Hits++;
	    if(pixelCluster->auxdataConst< int >( "isSplit" ) !=0) hitSplitL2++;	  
	    if(pixelCluster->isAvailable<std::vector<float>>("rdo_charge")){
	      for(int ip=0; ip<(int)rdo_charge(*pixelCluster).size(); ip++){
		if(rdo_charge(*pixelCluster).at(ip) !=0){
		  clus_pixelL2Charge.push_back((int)(rdo_charge(*pixelCluster).at(ip)));
		  clus_pixelL2Row.push_back((int)(rdo_phi_pixel_index(*pixelCluster).at(ip)));
		  clus_pixelL2Col.push_back((int)(rdo_eta_pixel_index(*pixelCluster).at(ip)));
		  clus_pixelL2Index.push_back(ip);
		  if(pixelCluster->isAvailable<std::vector<int>>("rdo_tot") && (int)rdo_tot(*pixelCluster).size() > ip && rdo_tot(*pixelCluster).at(ip) !=0){
		    clus_pixelL2ToT.push_back((int)(rdo_tot(*pixelCluster).at(ip)));
		  }else{
		    clus_pixelL2ToT.push_back(-1);
		  }
		}
	      }
	      
	    }else if(hasPixelNN){
	      
	      for(int ip=0; ip<(int)NN_matrixOfCharge(*pixelCluster ).size(); ip++){
		if(NN_matrixOfCharge(*pixelCluster).at(ip) !=0){
		  
		  clus_pixelL2Charge.push_back((int)NN_matrixOfCharge(*pixelCluster).at(ip));
		  clus_pixelL2Index.push_back(ip);
		  if(hasPixelToT&&(int)NN_matrixOfToT(*pixelCluster).size()>ip&&NN_matrixOfToT(*pixelCluster).at(ip) !=0){
		    clus_pixelL2ToT.push_back((int)NN_matrixOfToT(*pixelCluster).at(ip));
		  }
		}
	      }
	    }
	    
	  }else if ( layer==3 ){
	    
	    nL3Hits++;
	    if(pixelCluster->auxdataConst< int >( "isSplit" ) !=0) hitSplitL3++;
	    
	    if(pixelCluster->isAvailable<std::vector<float>>("rdo_charge")){
	      for(int ip=0; ip<(int)rdo_tot(*pixelCluster).size(); ip++){
		if(rdo_charge(*pixelCluster).at(ip) !=0){
		  clus_pixelL3Charge.push_back((int)(rdo_charge(*pixelCluster).at(ip)));
		  clus_pixelL3Row.push_back((int)(rdo_phi_pixel_index(*pixelCluster).at(ip)));
		  clus_pixelL3Col.push_back((int)(rdo_eta_pixel_index(*pixelCluster).at(ip)));
		  clus_pixelL3Index.push_back(ip);
		  if(pixelCluster->isAvailable<std::vector<int>>("rdo_tot") && (int)rdo_tot(*pixelCluster).size() > ip && rdo_tot(*pixelCluster).at(ip) !=0){
		    clus_pixelL3ToT.push_back((int)(rdo_tot(*pixelCluster).at(ip)));
		  }else{
		    clus_pixelL3ToT.push_back(-1);
		  }
		}
	      }
	      
	    }else if(hasPixelNN){

	      for(int ip=0; ip<(int)NN_matrixOfCharge(*pixelCluster ).size(); ip++){
		if(NN_matrixOfCharge(*pixelCluster).at(ip) !=0){
		  
		  clus_pixelL3Charge.push_back((int)NN_matrixOfCharge(*pixelCluster).at(ip));
		  clus_pixelL3Index.push_back(ip);
		  if(hasPixelToT&&(int)NN_matrixOfToT(*pixelCluster).size()>ip&&NN_matrixOfToT(*pixelCluster).at(ip) !=0){
		    clus_pixelL3ToT.push_back((int)NN_matrixOfToT(*pixelCluster).at(ip));
		  }
		}
	      }
	    }
	  }
	}
      
	//	  std::cout << nL0Hits << " " << nIBLHits << " " << nL1Hits << " " << nBLHits << std::endl;
	
	//	  if(nIBLHits<2 && nBLHits<2 && nL2Hits < 2 && nL3Hits <2) continue;
	
	hitLayer.push_back(layer);
	hitIsEndCap.push_back(bec);
	if(pixelCluster->auxdataConst< int >( "isSplit" ) !=0){
	  hitIsSplit.push_back(1);
	}else{
	  hitIsSplit.push_back(0);
	}
	hitSplitProb1.push_back(pixelCluster->auxdata<float>("splitProbability1"));
	hitSplitProb2.push_back(pixelCluster->auxdata<float>("splitProbability2"));
	//	hitSplitProb3.push_back(pixelCluster->auxdata<float>("splitProbability3"));

	hitIsHole.push_back(0);
	hitIsOutlier.push_back(0);

	pitchX = 0.050;
	pitchY = 0.400;
	  
	if(layer == 0 && bec == 0){
	  pitchX = 0.050;
	  hitIsIBL.push_back(1);
	  if((pixelCluster->auxdata< int >("eta_module")) <=5 && (pixelCluster->auxdata< int >("eta_module")) >=-6){
	    hitIs3D.push_back(0);
	  }else{
	    hitIs3D.push_back(1);
	    n3DHits++;

	    float minDiff3DX = 1000.;
	    float minDiff3DY = 1000.;

	    for( auto  allCluster_itr = pixelClusters->begin(); allCluster_itr != pixelClusters->end(); allCluster_itr++){  
      
	      const xAOD::TrackMeasurementValidation* allPixelCluster =  *allCluster_itr;        

	      if(allPixelCluster->auxdata< int>("layer") == 0 && (allPixelCluster->auxdata< int >("phi_module")) == (pixelCluster->auxdata< int >("phi_module")) &&  (allPixelCluster->auxdata< int >("eta_module")) ==  (pixelCluster->auxdata< int >("eta_module")) && allPixelCluster != pixelCluster){

		if(abs(pixelCluster->localX() - allPixelCluster->localX()) < 0.25){
		  float diff3DX = pixelCluster->localX() - allPixelCluster->localX();
		  float diff3DY = pixelCluster->localY() - allPixelCluster->localY();
		  if(sqrt(diff3DX*diff3DX + diff3DY*diff3DY) < sqrt(minDiff3DX*minDiff3DX + minDiff3DY*minDiff3DY)){
		    minDiff3DX = diff3DX;
		    minDiff3DY = diff3DY;
		  } 
		}
	      }
	    }
	    //	    if(minDiff3DX<100.)std::cout << minDiff3DX << " " << minDiff3DY << std::endl;
	    hit3DClusterDX.push_back(minDiff3DX);
	    hit3DClusterDY.push_back(minDiff3DY);
	  }
	}else{
	  hitIsIBL.push_back(0);
	  hitIs3D.push_back(0);
	} 

	// cluster position used in track fit
	float clusLocalX = msos->biasedResidualX() + msos->localX();
	float clusLocalY = msos->biasedResidualY() + msos->localY();

	// unbiased track position
	float trkUnbiasedLocX = clusLocalX - msos->unbiasedResidualX();
	float trkUnbiasedLocY = clusLocalY - msos->unbiasedResidualY();

	//	std::cout<< "hitLocalX " << clusLocalX << " " << msos->auxdata< float>("clusX") << std::endl;
	//	std::cout<< "hitLocalY " << clusLocalY << " " << msos->auxdata< float>("clusY") << std::endl;
	
	// all the errors we will solve for using a bit more math
	float trkBiasedCovXY(-999);
	float trkBiasedCovXX(-999);
	float trkBiasedCovYY(-999);
	float clusCovXX(-999);
	float clusCovYY(-999);
	float trkUnbiasedCovXX(-999);
	float trkUnbiasedCovYY(-999);

	bool isGood = SolveForErrorsAndLocations(
        // inputs
        msos->biasedResidualX(),  msos->biasedResidualY(), 
        msos->biasedPullX(),      msos->biasedPullY(), 
        msos->unbiasedResidualX(),msos->unbiasedResidualY(),
        msos->unbiasedPullX(),    msos->unbiasedPullY(),
        msos->localX(),           msos->localY(),
        trkUnbiasedLocX,          trkUnbiasedLocY,
        true, // first time calling the function
        // outputs
        trkBiasedCovXY,trkBiasedCovXX,trkBiasedCovYY,
        clusCovXX,clusCovYY,
        trkUnbiasedCovXX,trkUnbiasedCovYY);
      
	//if(isGood)std::cout << bec << " Layer " << layer << " Clus Local X " << clusLocalX << " Y " <<  clusLocalY << " " << msos->auxdata< float > ("clusX") << " " << msos->auxdata< float > ("clusY") << " " << sqrt(clusCovXX) << " " << sqrt(clusCovYY) << " " << msos->auxdata< float > ("clusErrorX") << " " << msos->auxdata< float > ("clusErrorY") << std::endl;

	//	std::cout <<  clusLocalX << " " << clusCovXX << " " << msos->auxdata<float>("clusX") << " " << msos->auxdata<float>("clusErrorX") << std::endl;

	trkPhiOnSurface.push_back(msos->auxdata< float > ("localPhi"));
	trkThetaOnSurface.push_back(msos->auxdata< float > ("localTheta"));    

	hitGlobalX.push_back(pixelCluster->auxdata< float >("globalX"));
	hitGlobalY.push_back(pixelCluster->auxdata< float >("globalY"));
	hitGlobalZ.push_back(pixelCluster->auxdata< float >("globalZ"));
      
	if(bec==0){
	  if(layer==0){
	    hitIBL.SetXYZ(pixelCluster->auxdata< float >("globalX"),pixelCluster->auxdata< float >("globalY"),pixelCluster->auxdata< float >("globalZ"));
	    hasIBL = true;
	  }else if(layer==1){
	    hitBLayer.SetXYZ(pixelCluster->auxdata< float >("globalX"),pixelCluster->auxdata< float >("globalY"),pixelCluster->auxdata< float >("globalZ"));
	    hasBLayer = true;
	  }else if(layer==2){
	    hitLayer1.SetXYZ(pixelCluster->auxdata< float >("globalX"),pixelCluster->auxdata< float >("globalY"),pixelCluster->auxdata< float >("globalZ"));
	    hasLayer1 = true;
	  }else if(layer==3){
	    hitLayer2.SetXYZ(pixelCluster->auxdata< float >("globalX"),pixelCluster->auxdata< float >("globalY"),pixelCluster->auxdata< float >("globalZ"));
	    hasLayer2 = true;
	  }
	}
	
	hitLocalX.push_back(clusLocalX);
	hitLocalY.push_back(clusLocalY);
	clustLocalX.push_back(pixelCluster->localX());
	clustLocalY.push_back(pixelCluster->localY());
	trkLocalX.push_back(trkUnbiasedLocX);
	trkLocalY.push_back(trkUnbiasedLocY);

	if(isGood){
	  hitLocalErrorX.push_back(sqrt(clusCovXX));
	  hitLocalErrorY.push_back(sqrt(clusCovYY));
	  trkLocalErrorX.push_back(sqrt(trkUnbiasedCovXX));
	  trkLocalErrorY.push_back(sqrt(trkUnbiasedCovYY));
	}else{
	  hitLocalErrorX.push_back(-999);
	  hitLocalErrorY.push_back(-999);
	  trkLocalErrorX.push_back(-999);
	  trkLocalErrorY.push_back(-999);
	}

	hitCharge.push_back(pixelCluster->auxdata< float >("charge"));
	hitToT.push_back(pixelCluster->auxdata< int >("ToT"));
	hitLVL1A.push_back(pixelCluster->auxdata< int >("LVL1A"));
	hitNPixel.push_back(pixelCluster->auxdataConst< int>("nRDO"));
	hitNPixelX.push_back(pixelCluster->auxdataConst< int >( "sizePhi" ));
	hitNPixelY.push_back(pixelCluster->auxdataConst< int >( "sizeZ" ));
	hitEtaModule.push_back(pixelCluster->auxdata< int >("eta_module"));
	hitPhiModule.push_back(pixelCluster->auxdata< int >("phi_module"));

	contributingPtcsBarcode.clear();
	contributingPtcsPdgId.clear();
	contributingPtcsPt.clear();
	contributingPtcsE.clear();

	int nPUinCluster = 0;
	bool hasTruth = false;

	if(pixelCluster->isAvailable<std::vector<int>>("truth_barcode")){
	  
	  for(int ip=0; ip<(int)truth_barcode(*pixelCluster ).size(); ip++){
	    if(truth_barcode(*pixelCluster).at(ip) > 2.e5)nPUinCluster++;
	    for( auto truthPtc_itr = truthPtcs->begin(); truthPtc_itr != truthPtcs->end(); truthPtc_itr++) {
	      if((*truthPtc_itr)->barcode() == truth_barcode(*pixelCluster).at(ip)){
		hasTruth = true;
		contributingPtcsBarcode.push_back((*truthPtc_itr)->barcode());
		contributingPtcsPdgId.push_back((*truthPtc_itr)->pdgId());
		contributingPtcsE.push_back((*truthPtc_itr)->e()/1000.);
		contributingPtcsPt.push_back((*truthPtc_itr)->pt()/1000.);
		break;
		//std::cout << ip << " " << (int)pixelCluster->auxdataConst< int>("nRDO") << " " << (*truthPtc_itr)->barcode() << " " << (*truthPtc_itr)->pt()/1000. << std::endl;
	      }
	    }
	    if(hasTruth==false){
	      contributingPtcsBarcode.push_back(-999);
	      contributingPtcsPdgId.push_back(-999);
	      contributingPtcsE.push_back(-999);
	      contributingPtcsPt.push_back(-999);
	    }
	  }
	  clus_nContributingPtcs.push_back((int)truth_barcode(*pixelCluster).size());
	  clus_nContributingPU.push_back(nPUinCluster);
	  clus_contributingPtcsBarcode.push_back(contributingPtcsBarcode);
	  clus_contributingPtcsPdgId.push_back(contributingPtcsPdgId);
	  clus_contributingPtcsE.push_back(contributingPtcsE);
	  clus_contributingPtcsPt.push_back(contributingPtcsPt);

	}else if(pixelCluster->isAvailable<std::vector<int>>("NN_barcode")){
	    
	  for(int ip=0; ip<(int)NN_barcode(*pixelCluster ).size(); ip++){
	    if(NN_barcode(*pixelCluster).at(ip) > 2.e5)nPUinCluster++;
	    //	    contributingPtcsBarcode.push_back((int)NN_barcode(*pixelCluster ).at(ip));
	    for( auto truthPtc_itr = truthPtcs->begin(); truthPtc_itr != truthPtcs->end(); truthPtc_itr++) {
	      if((*truthPtc_itr)->barcode() == NN_barcode(*pixelCluster).at(ip)){
		hasTruth = true;
		contributingPtcsBarcode.push_back((*truthPtc_itr)->barcode());
		contributingPtcsPdgId.push_back((*truthPtc_itr)->pdgId());
		contributingPtcsE.push_back((*truthPtc_itr)->e()/1000.);
		contributingPtcsPt.push_back((*truthPtc_itr)->pt()/1000.);
	      }
	    }
	    if(hasTruth==false){
	      contributingPtcsBarcode.push_back(-999);
	      contributingPtcsPdgId.push_back(-999);
	      contributingPtcsE.push_back(-999);
	      contributingPtcsPt.push_back(-999);
	    }
	  }
	  clus_nContributingPtcs.push_back((int)NN_barcode(*pixelCluster ).size());
	  clus_nContributingPU.push_back(nPUinCluster);
	  clus_contributingPtcsBarcode.push_back(contributingPtcsBarcode);
	  clus_contributingPtcsPdgId.push_back(contributingPtcsPdgId);
	  clus_contributingPtcsE.push_back(contributingPtcsE);
	  clus_contributingPtcsPt.push_back(contributingPtcsPt);
	}

	if(iblMCRadHV>0 || blMCRadHV>0 || ibl3dMCRadHV>0){
	  //if(iblMCRadHV>0 || blMCRadHV>0){
	  if(layer == 0 && bec == 0){ 
	    if((pixelCluster->auxdata< int >("eta_module")) <=5 && (pixelCluster->auxdata< int >("eta_module")) >=-6){
	      if(iblMCRadHV>0)hitVBias.push_back(-iblMCRadHV);
	    }else{ 
	      if(ibl3dMCRadHV>0)hitVBias.push_back(-ibl3dMCRadHV); 
	    }
	  }
	  if(blMCRadHV>0 && layer == 1 && bec == 0){
	    hitVBias.push_back(-blMCRadHV);
	  }
	}else{
	  if(pixelCluster->isAvailable< float >( "BiasVoltage" ) ){
	    hitVBias.push_back(pixelCluster->auxdata< float >("BiasVoltage"));
	  }else{
	    hitVBias.push_back(-1);
	  }
	}
	if(pixelCluster->isAvailable< float >( "DepletionVoltage" ) ){
	  hitVDep.push_back(pixelCluster->auxdata< float >("DepletionVoltage"));
	}else{
	  hitVDep.push_back(-1);
	}
	if(pixelCluster->isAvailable< float >( "Temperature" ) ){
	  hitTemp.push_back(pixelCluster->auxdata< float >("Temperature"));
	}else{
	  hitTemp.push_back(-1);
	}
	if(pixelCluster->isAvailable< float >( "LorentzShift" ) ){
	  hitLorentzShift.push_back(pixelCluster->auxdata< float >("LorentzShift"));
	}else{
	  hitLorentzShift.push_back(-1);
	}
	
	//	    hitTanLorentzAngle.push_back(pixelCluster->auxdata< float >("ExpectedLorentzShiftAtModule"));
	
	if(hasRefit){
	  float resiRX  = msos->auxdata< float > ("unbiasedResidualRefitX");
	  float resiRY  = msos->auxdata< float > ("unbiasedResidualRefitY");
	  float bresiRX  = msos->auxdata< float > ("biasedResidualRefitX");
	  float bresiRY  = msos->auxdata< float > ("biasedResidualRefitY");
	  float resiRXErr  = msos->auxdata< float > ("unbiasedResidualRefitXErr");
	  float resiRYErr  = msos->auxdata< float > ("unbiasedResidualRefitYErr");
	  float bresiRXErr  = msos->auxdata< float > ("biasedResidualRefitXErr");
	  float bresiRYErr  = msos->auxdata< float > ("biasedResidualRefitYErr");
	  unbiasedResidualRefitX.push_back(resiRX);
	  unbiasedResidualRefitY.push_back(resiRY);
	  biasedResidualRefitX.push_back(bresiRX);
	  biasedResidualRefitY.push_back(bresiRY);
	  unbiasedResidualRefitXErr.push_back(resiRXErr);
	  unbiasedResidualRefitYErr.push_back(resiRYErr);
	  biasedResidualRefitXErr.push_back(bresiRXErr);
	  biasedResidualRefitYErr.push_back(bresiRYErr);
	  float measX = 0.;
	  float measY = 0.;
	  for(unsigned int i = 0; i < pixelLayer.size(); i++){
	    if(pixelLayer.at(i) == layer && i < trkHitLocalX.size()){
	      measX = trkHitLocalX.at(i);
	      measY = trkHitLocalY.at(i);
	    }
	  }
	  trkRefitLocalX.push_back(measX-resiRX);
	  trkRefitLocalY.push_back(measY-resiRY);
	}else{
	  unbiasedResidualRefitX.push_back(-999);
	  unbiasedResidualRefitY.push_back(-999);
	  biasedResidualRefitX.push_back(-999);
	  biasedResidualRefitY.push_back(-999);
	  unbiasedResidualRefitXErr.push_back(-999);
	  unbiasedResidualRefitYErr.push_back(-999);
	  biasedResidualRefitXErr.push_back(-999);
	  biasedResidualRefitYErr.push_back(-999);
	  trkRefitLocalX.push_back(-999);
	  trkRefitLocalY.push_back(-999);
	}
	
	float resiX  = msos->auxdata< float > ("unbiasedResidualX");
	float resiY  = msos->auxdata< float > ("unbiasedResidualY");
	float pullX  = msos->auxdata< float > ("unbiasedPullX");
	float pullY  = msos->auxdata< float > ("unbiasedPullY");
	
	float resibX = msos->auxdata< float > ("biasedResidualX");
	float resibY = msos->auxdata< float > ("biasedResidualY");
	float pullbX = msos->auxdata< float > ("biasedPullX");
	float pullbY = msos->auxdata< float > ("biasedPullY");
	
	theTrkExtr.Set(trkUnbiasedLocX,trkUnbiasedLocY);
	//	hitLocalErrorX.push_back(sqrt((pow(resiX/pullX,2) + pow(resibX/pullbX,2))/2.));
	//	hitLocalErrorY.push_back(sqrt((pow(resiY/pullY,2) + pow(resibY/pullbY,2))/2.));
	//	trkLocalErrorX.push_back(sqrt((pow(resiX/pullX,2) - pow(resibX/pullbX,2))/2.));
	//	trkLocalErrorY.push_back(sqrt((pow(resiY/pullY,2) - pow(resibY/pullbY,2))/2.));

	//	trkLocalX.push_back(pixelCluster->localX()-resiX);
	//	trkLocalY.push_back(pixelCluster->localY()-resiY);
	  

	unbiasedResidualX.push_back(resiX);
	unbiasedResidualY.push_back(resiY);
	unbiasedPullX.push_back(pullX);
	unbiasedPullY.push_back(pullY);
	biasedResidualX.push_back(resibX);
	biasedResidualY.push_back(resibY);
	biasedPullX.push_back(pullbX);
	biasedPullY.push_back(pullbY);
	
	if(isMC && hasSiHits && truebarcode != -1){
	  
	  static SG::AuxElement::ConstAccessor< std::vector<int> >     acc_sihit_barcode("sihit_barcode");
	  static SG::AuxElement::ConstAccessor< std::vector<int> >     acc_sihit_pdgid("sihit_pdgid");
	  static SG::AuxElement::ConstAccessor< std::vector<float> >   acc_sihit_startPosX("sihit_startPosX");
	  static SG::AuxElement::ConstAccessor< std::vector<float> >   acc_sihit_startPosY("sihit_startPosY");
	  static SG::AuxElement::ConstAccessor< std::vector<float> >   acc_sihit_endPosX("sihit_endPosX");
	  static SG::AuxElement::ConstAccessor< std::vector<float> >   acc_sihit_endPosY("sihit_endPosY");
	  static SG::AuxElement::ConstAccessor< std::vector<float> >   acc_sihit_energyDeposit("sihit_energyDeposit");
	  
	  
	  const std::vector<int>   &  sihit_barcode   = acc_sihit_barcode(*pixelCluster);
	  const std::vector<int>   &  sihit_pdgid   = acc_sihit_pdgid(*pixelCluster);
	  const std::vector<float> &  sihit_startPosX = acc_sihit_startPosX(*pixelCluster);
	  const std::vector<float> &  sihit_startPosY = acc_sihit_startPosY(*pixelCluster);
	  const std::vector<float> &  sihit_endPosX   = acc_sihit_endPosX(*pixelCluster);
	  const std::vector<float> &  sihit_endPosY   = acc_sihit_endPosY(*pixelCluster);
	  const std::vector<float> &  sihit_energyDeposit = acc_sihit_energyDeposit(*pixelCluster);
	  
	  int myTP_Location(-1);
	  
	  for( unsigned int i(0); i < sihit_barcode.size(); ++i ){
	    if( sihit_barcode[i] == truebarcode ){
	      myTP_Location = i;
	      break;
	    }
	  }
	  
	  //Cluster is incorrectly assigned to the track
	  if(myTP_Location == -1){
	    g4LocalX.push_back(-999.);
	    g4LocalY.push_back(-999.);
	    g4EntryLocalX.push_back(-999);
	    g4EntryLocalY.push_back(-999);
	    g4ExitLocalX.push_back(-999);
	    g4ExitLocalY.push_back(-999);
	    g4Barcode.push_back(-999);
	    g4PdgId.push_back(-999);
	    g4EnergyDeposit.push_back(-999);
	    continue;
	  }
	  
	  float hitLocalXG4 = (sihit_endPosX[myTP_Location] + sihit_startPosX[myTP_Location]) *0.5;
	  float hitLocalYG4 = (sihit_endPosY[myTP_Location] + sihit_startPosY[myTP_Location]) *0.5;
	  float entryLocalXG4 = sihit_startPosX[myTP_Location];
	  float entryLocalYG4 = sihit_startPosY[myTP_Location];
	  float exitLocalXG4 = sihit_endPosX[myTP_Location];
	  float exitLocalYG4 = sihit_endPosY[myTP_Location];
	  int hitBarcode =  sihit_barcode[myTP_Location];
	  int hitPdgId =  sihit_pdgid[myTP_Location];
	  float hitEnergyDeposit =  sihit_energyDeposit[myTP_Location];
	  
	  if(layer==0){
	    hitLocalXG4IBL = hitLocalXG4;
	    hitLocalYG4IBL = hitLocalYG4;
	  }
	  
	  g4LocalX.push_back(hitLocalXG4);
	  g4LocalY.push_back(hitLocalYG4);
	  g4EntryLocalX.push_back(entryLocalXG4);
	  g4EntryLocalY.push_back(entryLocalYG4);
	  g4ExitLocalX.push_back(exitLocalXG4);
	  g4ExitLocalY.push_back(exitLocalYG4);
	  g4Barcode.push_back(hitBarcode);
	  g4PdgId.push_back(hitPdgId);
	  g4EnergyDeposit.push_back(hitEnergyDeposit);
	  
	  minDistIBL = 999;
	  
	  std::vector<int> otherParticles;  
	  for( unsigned int i(0); i < sihit_barcode.size(); ++i ){
	    if( (int)i == myTP_Location){
	      continue;
	    }
	    //only consider primary particles
	    if( sihit_barcode[i] >0  && sihit_barcode[i]< 200000 ){
	      otherParticles.push_back(i);
	    }
	  }	    
	  
	  //Multiple particles in the cluster which is the closest one 
	  if (otherParticles.size() > 0 ){
	    float minDeltaR(100000);
	    float minDeltaX = 999;
	    float minDeltaY = 999;
	    
	    //	      int closest(0);
	    for( auto i : otherParticles ){
	      float dx = hitLocalXG4 - (sihit_endPosX[i] + sihit_startPosX[i]) *0.5;
	      float dy = hitLocalYG4 - (sihit_endPosY[i] + sihit_startPosY[i]) *0.5;
	      float deltaR = sqrt(dx*dx + dy*dy);
	      if(deltaR < minDeltaR){
		//		  closest =  (int)i;
		minDeltaR = deltaR;
		minDeltaX = fabs(dx);
		minDeltaY = fabs(dy);
	      }
	    }
	    if(layer==0)minDistIBL = minDeltaR;
	    minTrkG4DistX.push_back(minDeltaX);
	    minTrkG4DistY.push_back(minDeltaY);
	  }else{
	    minTrkG4DistX.push_back(999.);
	    minTrkG4DistY.push_back(999.);
	  }
	  
	  /*
	  // Loop over track MeasurementStateOnSurface's
	  
	  for( auto msos_iter2nd = measurementsOnTrack.begin(); msos_iter2nd != measurementsOnTrack.end(); ++msos_iter2nd){  
	  //Check if the element link is valid
	  if( ! (*msos_iter2nd).isValid() ) {
	  continue;
	  }
	  
	  const xAOD::TrackStateValidation* msos2nd = *(*msos_iter2nd); 
	  
	  if( msos2nd->detType() != TrackState::Pixel  ||  msos2nd->type() != TrackStateOnSurface::Measurement )  //its   pixel && and a measurement on track
	  continue;
	  
	  //Get pixel cluster
	  if(  msos2nd->trackMeasurementValidationLink().isValid() && *(msos2nd->trackMeasurementValidationLink()) ){
	  const xAOD::TrackMeasurementValidation* pixelCluster2nd =  *(msos2nd->trackMeasurementValidationLink());        
	  
	  int bec2 = pixelCluster2nd->auxdata< int>("bec");
	  int layer2 = pixelCluster2nd->auxdata< int>("layer");
	  if(bec2 == 0 && layer2 == layer){
	  
	  //		  if(pixelCluster2nd != pixelCluster){
	  
	  const std::vector<int>   &  sihit_barcode2   = acc_sihit_barcode(*pixelCluster2nd);
	  const std::vector<float> &  sihit_startPosX2 = acc_sihit_startPosX(*pixelCluster2nd);
	  const std::vector<float> &  sihit_startPosY2 = acc_sihit_startPosY(*pixelCluster2nd);
	  const std::vector<float> &  sihit_endPosX2   = acc_sihit_endPosX(*pixelCluster2nd);
	  const std::vector<float> &  sihit_endPosY2   = acc_sihit_endPosY(*pixelCluster2nd);
	  
	  for( unsigned int i(0); i < sihit_barcode.size(); ++i ){
	  if( sihit_barcode2[i] != truebarcode){
	  float dist = sqrt(pow((sihit_endPosX2[i] + sihit_startPosX2[i])*0.5-hitLocalXG4,2)+pow((sihit_endPosY2[i] + sihit_startPosY2[i])*0.5-hitLocalYG4,2));
	  if(dist < minDist && layer==0) minDist = dist;
	  }
	  }
	  //		  }
	  }
	  }
	  }
	  */
	}else{
	  g4LocalX.push_back(-999.);
	  g4LocalY.push_back(-999.);
	  g4EntryLocalX.push_back(-999.);
	  g4EntryLocalY.push_back(-999.);
	  g4ExitLocalX.push_back(-999.);
	  g4ExitLocalY.push_back(-999.);
	}
	
        int nTrkDens10 = 1;
        int nTrkDens25 = 1;
        int nTrkDens50 = 1;
        int nTrkDensPitch2 = 1;
        int nTrkDensPitch5 = 1;
        int nTrkDensPitch10 = 1;
	float minDeltaTrk(100000);
	float minDeltaX = 999;
	float minDeltaY = 999;
	mindR = 9999.;
	
	if(doDensity){
	  for( auto recoNextTrk_itr = recoTracks->begin(); recoNextTrk_itr != recoTracks->end(); recoNextTrk_itr++) {
	    if((*recoNextTrk_itr)==(*recoTrk_itr)) continue;
	    
	    float dr = 0.; 
	    float dEta = (*recoNextTrk_itr)->eta() - (*recoTrk_itr)->eta();
	    float dPhi = (*recoNextTrk_itr)->phi() - (*recoTrk_itr)->phi();
	    dr =  sqrtf(dEta*dEta + dPhi*dPhi);
	    if(dr<mindR && dr>1.e-3) {mindR = dr;}
	    
	    if((*recoNextTrk_itr)->pt()/1000. > 0.25 && dr < 0.85){
	      
	      if( ! (*recoNextTrk_itr)->isAvailable< MeasurementsOnTrack >( measurementNames ) ) {
		
		Info("execute()",  "no MSOS Available");
		
		continue;
	      }
	      
	      const MeasurementsOnTrack& measurementsOnNextTrack = (*recoNextTrk_itr)->auxdata< MeasurementsOnTrack >( measurementNames );
	      
	      for( auto msosnext_iter = measurementsOnNextTrack.begin(); msosnext_iter != measurementsOnNextTrack.end(); ++msosnext_iter){  
		if( ! (*msosnext_iter).isValid() ) {
		  continue;
		}
		
		const xAOD::TrackStateValidation* msosnext = *(*msosnext_iter);  
		if( msosnext->detType() != TrackState::Pixel  ||  msosnext->type() != TrackStateOnSurface::Measurement )
		  continue;
		
		//Get pixel cluster
		if(msosnext->trackMeasurementValidationLink().isValid() && *(msosnext->trackMeasurementValidationLink()) ){
		  const xAOD::TrackMeasurementValidation* thisPixelCluster =  *(msosnext->trackMeasurementValidationLink());        
		  
		  int thisBec = thisPixelCluster->auxdata< int>("bec");
		  int thisLayer = thisPixelCluster->auxdata< int>("layer");
		  if(thisBec == 0){
		    if(thisLayer==layer && thisPixelCluster->auxdataConst< int >("eta_module") == etaModule && pixelCluster->auxdataConst< int >("phi_module") == phiModule){
		      // unbiased track position
		      float trkUnbiasedNextLocX = msosnext->biasedResidualX() + msosnext->localX() - msosnext->unbiasedResidualX();
		      float trkUnbiasedNextLocY = msosnext->biasedResidualY() + msosnext->localY() - msosnext->unbiasedResidualY();
		      
		      float distTrkX = fabs(trkUnbiasedNextLocX-theTrkExtr.X());
		      float distTrkY = fabs(trkUnbiasedNextLocY-theTrkExtr.Y());
		      float distTrk = sqrt(distTrkX*distTrkX+distTrkY*distTrkY);
		      float distTrkNorm = sqrt((distTrkX*distTrkX)/(pitchX*pitchX)+(distTrkY*distTrkY)/(pitchY*pitchY));
		      if(distTrk > 2.e-3){
			if(distTrk < minDeltaTrk){
			  minDeltaTrk = distTrk;
			  minDeltaX = distTrkX;
			  minDeltaY = distTrkY;
			}
			if(distTrk < 1.0)nTrkDens10++;
			if(distTrk < 2.5)nTrkDens25++;
			if(distTrk < 5.0)nTrkDens50++;
			if(distTrkNorm < 2.0)nTrkDensPitch2++;
			if(distTrkNorm < 5.0)nTrkDensPitch5++;
			if(distTrkNorm < 10.0)nTrkDensPitch10++;
		      }
		    }
		  }
		}
	      }
	    }
	  }
	
	  int nHitDens10 = 1;
	  int nHitDens25 = 1;
	  int nHitDens50 = 1;
	  int nHitDensPitch2 = 1;
	  int nHitDensPitch5 = 1;
	  int nHitDensPitch10 = 1;
	  
	  for( auto  nextcluster_itr = pixelClusters->begin(); nextcluster_itr != pixelClusters->end(); nextcluster_itr++){  
	    
	    if(layer == (*nextcluster_itr)->auxdataConst< int>("layer") && phiModule == (*nextcluster_itr)->auxdata< int >("phi_module") && etaModule == (*nextcluster_itr)->auxdata< int >("eta_module") && bec == (*nextcluster_itr)->auxdataConst< int>("bec")){
	      
	      float clusDist = sqrt(pow(pixelCluster->localX()-(*nextcluster_itr)->localX(),2)+pow(pixelCluster->localY()-(*nextcluster_itr)->localY(),2));
	      
	      if(clusDist<1.0)nHitDens10++;
	      if(clusDist<2.5)nHitDens25++;
	      if(clusDist<5.0)nHitDens50++;
	    }
	  }

	  minTrkDistX.push_back(minDeltaX);		
	  minTrkDistY.push_back(minDeltaY);	
	  trkDens10.push_back(nTrkDens10/(TMath::Pi()));	
	  trkDens25.push_back(nTrkDens25/(2.5*2.5*TMath::Pi()));	
	  trkDens50.push_back(nTrkDens50/(5.0*5.0*TMath::Pi()));
	  trkDensPitch2.push_back(nTrkDensPitch2/(4.*pitchX*pitchY*TMath::Pi()));
	  trkDensPitch5.push_back(nTrkDensPitch5/(25.*pitchX*pitchY*TMath::Pi()));
	  trkDensPitch10.push_back(nTrkDensPitch10/(100.*pitchX*pitchY*TMath::Pi()));
	  
	  hitDens10.push_back(nHitDens10/(TMath::Pi()));	
	  hitDens25.push_back(nHitDens25/(2.5*2.5*TMath::Pi()));	
	  hitDens50.push_back(nHitDens50/(5.0*5.0*TMath::Pi()));

	}

      }
    }
  
    minDistToIBLHit = 999.;
    minDistToL2Hit = 999.;

    if(hasBLayer && hasLayer1){
      
      TVector3 lineHitToHit =  hitLayer1 - hitBLayer;	
      double trackLine[6] = {hitBLayer.x(), lineHitToHit.x(), hitBLayer.y(), lineHitToHit.y(), hitBLayer.z(), lineHitToHit.z()};
      
      //      std::cout << "3 Hits " << std::endl;

      for( auto  cluster_itr = pixelClusters->begin(); cluster_itr != pixelClusters->end(); cluster_itr++){  
	
	int bec = (*cluster_itr)->auxdataConst< int>("bec");
	int layer = (*cluster_itr)->auxdataConst< int>("layer");
	
	if(bec==0){
	  TVector3 thisHit((*cluster_itr)->globalX(),(*cluster_itr)->globalY(),(*cluster_itr)->globalZ());

	  if(layer==0){
	    float distToIBLHit = distTo3DLine(thisHit, trackLine);
	    if(distToIBLHit<minDistToIBLHit && distToIBLHit>0)minDistToIBLHit = distToIBLHit;
	  }else if(layer==3){
	    float distToL2Hit = distTo3DLine(thisHit, trackLine);
	    if(distToL2Hit<minDistToL2Hit && distToL2Hit>0)minDistToL2Hit = distToL2Hit;
	  }
	}
      }
    }
  
    //    std::cout << trackPt << " " << minTrackPt << " " << nIBLHits << std::endl;

    if(trackPt > minTrackPt && ((nIBLHits>1 || nBLHits>1 || nL2Hits>1 || nL3Hits>1) || !selectPixelOverlaps) && (n3DHits>0 || !select3DHits) && (nIBLHits>0 || !selectIBLHits) && ((trackJetPt>minJetPt && trackJetdR<0.5 ) || !doJets || minJetPt < 0.)){
      //      std::cout << "Write to trktree " << std::endl;
      m_trktree->Fill();
    }

  } // end for loop over reconstructed tracks
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackxAODAnalysis :: postExecute ()
{
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackxAODAnalysis :: finalize ()
{

  if(doClusterTree){
    std::cout << " " << std::endl;
    std::cout << "Nb of Processed Events " << m_eventCounter << " <Nb of Hits / Event>: IBL " << (float)nIntIBL/(float)m_eventCounter << " BL " << (float)nIntBL/(float)m_eventCounter << " L1 " << (float)nIntL1/(float)m_eventCounter << " L2 " << (float)nIntL2/(float)m_eventCounter << " EndCap " << (float)nIntEC/(float)m_eventCounter << std::endl;
  }

  /*
  std::cout << " " << std::endl;
  for(int i=0; i<m_trig_cnf; i++){
    std::cout << "Trigger " << m_HLT_Trig_name[i] << " Fired " << m_HLT_Trig_count[i] << std::endl;
  }
  */
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackxAODAnalysis :: histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}

float TrackxAODAnalysis::distTo3DLine(const TVector3 hit, const double *p) {
  
  TVector3 xp(hit.x(),hit.y(),hit.z());
  TVector3 x0(p[0], p[2], p[4] );
  TVector3 x1(p[0] + p[1], p[2] + p[3], p[4] + p[5]);
  TVector3 u = (x1-x0).Unit();
  double d2 = ((xp-x0).Cross(u)).Mag();

  return d2;
   
}

std::vector<float> TrackxAODAnalysis::trkD0ToVtx(const xAOD::TrackParticle* track, std::vector<float> &vtxXYZ){

  std::vector<float> results;

  double BField = 2.0; //Tesla           
  double aconst = -1.49898*BField;

  double qOverPt = track->qOverP()/sin(track->theta());
  double radiusOfCurvature = 10.*0.5/(qOverPt*aconst);

  double rinv = 1./radiusOfCurvature;
  double dca = track->d0();
  double phi0 = track->phi0();
  double cosPhi0 = cos(track->phi0());
  double sinPhi0 = sin(track->phi0());
  double xPos = -1.*(vtxXYZ[0]);
  double yPos = -1.*(vtxXYZ[1]);
  double dzds = tan(track->theta());

  const double u = 1. - rinv * dca;
  const double dp = -xPos * sinPhi0 + yPos * cosPhi0 + dca;
  const double dl = xPos * cosPhi0 + yPos * sinPhi0;
  const double sa = 2. * dp - rinv * (dp * dp + dl * dl);
  const double sb = rinv * xPos + u * sinPhi0;  //xC
  const double sc = -rinv * yPos + u * cosPhi0; //yC
  const double sd = sqrt(1. - rinv * sa);

  double newPhi0 = -1.;
  double newDca = -1.;
  double newZ0 = -1.;

  double pi =   3.1415926535;

  double sArc;
  if (rinv == 0.) {
    newDca = dp;
    sArc = dl;
  } else {
    newPhi0 = atan2(sb, sc);
    newDca = sa / (1. + sd);
    double dphi = newPhi0 - phi0;
    if (dphi > pi)
      dphi -= 2.0 * pi;
    else if (dphi < -pi)
      dphi += 2.0 * pi;
    sArc = dphi / rinv;
  }
  newZ0 += sArc * dzds;

  /*
  double U = radiusOfCurvature + track->d0();
  double xC = -1.*U*sin(track->phi0());  //Circle center x-coordinate
  double yC = U*cos(track->phi0());      //Circle center y-coordinate

  double radiusErr = 1.e-7*radiusOfCurvature; 
  double uErr = sqrt(pow(radiusErr,2)+track->definingParametersCovMatrix()(0,0));
  double xCErr = sqrt(pow(uErr/U,2)+pow(cos(track->phi0())/sin(track->phi0()),2)*track->definingParametersCovMatrix()(2,2))*fabs(xC);
  double yCErr = sqrt(pow(uErr/U,2)+pow(sin(track->phi0())/cos(track->phi0()),2)*track->definingParametersCovMatrix()(2,2))*fabs(yC);

  float d0ToVtx = sqrt(pow((vtxXYZ[0] - xC), 2) + pow((vtxXYZ[1] - yC), 2)) - fabs(radiusOfCurvature);
  if(track->charge()>0)d0ToVtx*=-1.;

  float d0ToVtxErr = sqrt(1./(pow((vtxXYZ[0] - xC), 2)+pow((vtxXYZ[2] - yC), 2)) * ( pow((vtxXYZ[0]- xC), 2)*(pow(xCErr,2)+ pow(vtxXYZ[1],2)) + pow((vtxXYZ[2] - yC), 2)*(pow(yCErr,2)+ pow(vtxXYZ[3],2))) + radiusErr*radiusErr);
  */

  //  std::cout << track->d0() << " " << newDca << " Vtx: (" << vtxXYZ[0] << ", " << vtxXYZ[1] << ", " << vtxXYZ[2] << ")" << std::endl;

  results.push_back(newDca);
  results.push_back(newZ0);
  //  results.push_back(d0ToVtx);
  //  results.push_back(d0ToVtxErr);

  return results;

}

std::pair<float,float> TrackxAODAnalysis::GetTrackBiasedCovXY(float rbx, float rby, float pbx, float pby, float rux, float pux, float trkbx, float trkux) {

  double a = std::pow(pbx,2)*std::pow(pby,2)*rbx*rby*std::pow(rux,2);
  double b = std::pow(pbx,2)*std::pow(pby,2)*std::pow(pux,2)*std::pow(rbx,2)*std::pow(rby,2);
  double c = (std::pow(pbx,2)-std::pow(pby,2))*std::pow(rux,2);
  double d = std::pow(pux,2)*std::pow(rbx+trkbx-trkux,2);
  double e = std::pow(rbx+trkbx-trkux,2);
  double f = std::pow(pbx,2)*std::pow(pby,2)*(std::pow(pby,2)*std::pow(rux,2)+std::pow(pux,2)*std::pow(rbx+trkbx-trkux,2));
  
  float res_a = -((a+sqrt(-b*(c-d)*e))/f);
  float res_b = (-a+sqrt(-b*(c-d)*e))/f;

  // return with smaller in abs value first
  if(fabs(res_a)<fabs(res_b)) {
    return {res_a, res_b};
  } else {
    return {res_b, res_a};
  }
}

float TrackxAODAnalysis::GetTrackBiasedCovXX(float rbx, float rby, float pbx, float pby, float trkbx, float trkux, float covTrkbxy) {
  float result = (-(covTrkbxy*std::pow(pby,2)*std::pow(rbx,2)*rby) + std::pow(rbx,2)*std::pow(rby,2)*(trkbx - trkux) - std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*(rbx + trkbx - trkux))/(std::pow(pbx,2)*rby*(covTrkbxy*std::pow(pby,2) + rbx*rby));
  
  return result;
}

float TrackxAODAnalysis::GetTrackBiasedCovYY(float rbx, float rby, float pbx, float pby, float trkby, float trkuy, float covTrkbxy) {
  float result = (-(covTrkbxy*std::pow(pbx,2)*rbx*std::pow(rby,2)) + std::pow(rbx,2)*std::pow(rby,2)*(trkby - trkuy) - std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*(rby + trkby - trkuy))/(std::pow(pby,2)*rbx*(covTrkbxy*std::pow(pbx,2) + rbx*rby));

  return result;
}

float TrackxAODAnalysis::GetClusterCovXX(float rbx, float pbx, float covTrkbxx) {
  float result = (std::pow(rbx,2)/std::pow(pbx,2)) + covTrkbxx;
  return result;
}

float TrackxAODAnalysis::GetClusterCovYY(float rby, float pby, float covTrkbyy) {
  float result = (std::pow(rby,2)/std::pow(pby,2)) + covTrkbyy;
  return result;
}

float TrackxAODAnalysis::GetTrackUnbiasedCovXX(float rbx, float rux, float pbx, float pux, float covTrkbxx) {
  float result = (std::pow(rux,2)/std::pow(pux,2)) - (std::pow(rbx,2)/std::pow(pbx,2)) - covTrkbxx;
//  covTrkuxx -> (covTrkbxy*std::pow(pbx,2)*std::pow(pby,2)*rby*std::pow(rux,2) + rbx*std::pow(rby,2)*(std::pow(pbx,2)*std::pow(rux,2) - std::pow(pux,2)*rbx*(rbx + trkbx - trkux)) + std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*std::pow(pux,2)*(rbx + trkbx - trkux))/(std::pow(pbx,2)*std::pow(pux,2)*rby*(covTrkbxy*std::pow(pby,2) + rbx*rby))
  return result;
}

float TrackxAODAnalysis::GetTrackUnbiasedCovYY(float rby, float ruy, float pby, float puy, float covTrkbyy) {
  float result = (std::pow(ruy,2)/std::pow(puy,2)) - (std::pow(rby,2)/std::pow(pby,2)) - covTrkbyy;
//  covTrkuyy -> (covTrkbxy*std::pow(pbx,2)*std::pow(pby,2)*rbx*std::pow(ruy,2) + std::pow(rbx,2)*rby*(std::pow(pby,2)*std::pow(ruy,2) - std::pow(puy,2)*rby*(rby + trkby - trkuy)) + std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*std::pow(puy,2)*(rby + trkby - trkuy))/(std::pow(pby,2)*std::pow(puy,2)*rbx*(covTrkbxy*std::pow(pbx,2) + rbx*rby))
  return result;
}

bool TrackxAODAnalysis::SolveForErrorsAndLocations(float rbx, float rby, float pbx, float pby, float rux, float ruy, float pux, float puy, float trkbx, float trkby, float trkux, float trkuy, bool firstPass, float& trkBiasedCovXY, float& trkBiasedCovXX, float& trkBiasedCovYY, float& clusCovXX, float& clusCovYY, float& trkUnbiasedCovXX, float& trkUnbiasedCovYY) {

  // first get the cross term 
  // there are two roots - take one, and get other parameters
  // if a diagonal element of any covariance matrix is negative, 
  // call this function again using the other solution for the cross-term
  // the smaller one tends to be correct, so start with that
  std::pair<float,float> covXYs = GetTrackBiasedCovXY(rbx, rby, pbx, pby, rux, pux, trkbx, trkux);
  if(firstPass) { trkBiasedCovXY = covXYs.first;  } // smaller one in abs value
  else          { trkBiasedCovXY = covXYs.second; } // larger  one in abs value

  // with that in hand, do the rest 
  // biased track errors in X and Y
  trkBiasedCovXX = GetTrackBiasedCovXX(rbx, rby, pbx, pby, trkbx, trkux, trkBiasedCovXY);
  trkBiasedCovYY = GetTrackBiasedCovYY(rbx, rby, pbx, pby, trkby, trkuy, trkBiasedCovXY);

  // cluster errors
  clusCovXX = GetClusterCovXX(rbx, pbx, trkBiasedCovXX);
  clusCovYY = GetClusterCovYY(rby, pby, trkBiasedCovYY);

  // complete the set and get the track unbiased error (only x and y, not the cross term)
  trkUnbiasedCovXX = GetTrackUnbiasedCovXX(rbx, rux, pbx, pux, trkBiasedCovXX);
  trkUnbiasedCovYY = GetTrackUnbiasedCovYY(rby, ruy, pby, puy, trkBiasedCovYY);

  bool isGood(true);

  // check for negatives where they cannot be (real numbers only here!)
  if(trkBiasedCovXX<0 || trkBiasedCovYY<0 ||
      clusCovXX<0 || clusCovYY<0 ||
      trkUnbiasedCovXX<0 || trkUnbiasedCovYY<0) {

    if(firstPass) {
      // call again with firstPass = false
      isGood = this->SolveForErrorsAndLocations(
          rbx, rby, pbx, pby, 
          rux, ruy, pux, puy, 
          trkbx, trkby, trkux, trkuy,
          false,
          trkBiasedCovXY, trkBiasedCovXX, trkBiasedCovYY,
          clusCovXX, clusCovYY, 
          trkUnbiasedCovXX, trkUnbiasedCovYY);
    } else { 
      // issue a warning and return false so user can decide what to do next
      //      ATH_MSG_WARNING("Negative solution for square of track or cluster errors");
      isGood = false;
//      std::cout << "\t" << trkBiasedCovXX << "\t" << trkBiasedCovYY << std::endl
//          << "\t" << clusCovXX << "\t" << clusCovYY << std::endl
//          << "\t" << trkUnbiasedCovXX << "\t" << trkUnbiasedCovYY << std::endl;
    }
  } // have negatives

  return isGood;
}

void TrackxAODAnalysis ::GetLayerEtaPhiFromId(uint64_t id){

  holeEtaModule =(id>>43) % 0x20 - 10;
  holePhiModule =(id>>43) % 0x800 / 0x20;
  int layer_index = ((id>>43) / 0x800) & 0xF;

  switch (layer_index) {
    case 4:
      holeBarrelEC=-2;
      holeLayer = 0;
      break;
    case 5:
      holeBarrelEC=-2;
      holeLayer = 1;
      break;
    case 6:
      holeBarrelEC=-2;
      holeLayer = 2;
      break;
    case 8:
      holeBarrelEC=0;
      holeLayer=0;
      break;
    case 9:
      holeBarrelEC=0;
      holeLayer=1;
      break;
    case 10:
      holeBarrelEC=0;
      holeLayer=2;
      break;
    case 11:
      holeBarrelEC=0;
      holeLayer =3;
      break;
    case 12:
      holeBarrelEC=2;
      holeLayer=0;
      break;
    case 13:
      holeBarrelEC=2;
      holeLayer =1;
      break;
    case 14:
      holeBarrelEC=2;
      holeLayer=2;
      break;
  }
  return;
}
