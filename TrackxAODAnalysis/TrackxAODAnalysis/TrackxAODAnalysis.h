#ifndef TrackxAODAnalysis_TrackxAODAnalysis_H
#define TrackxAODAnalysis_TrackxAODAnalysis_H

#include "TTree.h"
#include <TVector3.h>

#include "xAODTracking/TrackParticle.h" 

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <EventLoop/Algorithm.h>
#include "xAODEventInfo/EventInfo.h"

#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "AsgTools/AnaToolHandle.h"

class TrackxAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  TTree * m_trktree; //!
  TTree * m_splittrktree; //!
  TTree * m_clustertree; //!
  TTree * m_gentree; //!
  TTree * m_aodtree; //!

  std::string JetCollectionName;

  asg::AnaToolHandle<TrigConf::ITrigConfigTool>  m_trigConfTool_handle{"TrigConf::xAODConfigTool/xAODConfigTool" , this}; //!
  asg::AnaToolHandle<Trig::TrigDecisionTool>     m_trigDecTool_handle{"Trig::TrigDecisionTool/TrigDecisionTool" }; //!

  enum{m_trig_cnf = 11};
  TString m_HLT_Trig_name[m_trig_cnf];
  int     m_HLT_Trig_count[m_trig_cnf];
  int     m_HLT_Trig_fire[m_trig_cnf];

  bool m_HLT_j100;
  bool m_HLT_j150;
  bool m_HLT_j200;
  bool m_HLT_j300;
  bool m_HLT_j320;
  bool m_HLT_j360;
  bool m_HLT_j380;
  bool m_HLT_j400;
  bool m_HLT_j420;
  bool m_HLT_j460;
  bool m_HLT_MBTS;

  int lumiB;
  int bcid;
  int m_runNumber;
  int m_eventNumber;
  int m_timeStamp;
  int mcFlag;
  float m_averagePU;
  float m_eventPU;

  float bosonPt;

  int nPixelLayers;
  int nPixelHits;
  int nPixelHoles;
  int nPixelOutliers;
  int nSCTHits;
  int nSCTHoles;
  int nTRTHits;
  int nIBLHits;
  int nIBLSplitHits;
  int nIBLSharedHits;
  int nBLHits;
  int nBLSplitHits;
  int nBLSharedHits;
  int nL2SplitHits;
  int nL3SplitHits;
  int nECHits;
  int nL0Hits;
  int nL1Hits;
  int nL2Hits;
  int nL3Hits;
  int n3DHits;

  int nIBLHits2;
  int nBLHits2;
  int nPixelHits2;
  int nSCTHits2;

  int nIntEC;
  int nIntIBL;
  int nIntBL;
  int nIntL1;
  int nIntL2;

  int nIBLExpectedHits;
  int nBLExpectedHits;
  int nSCTSharedHits;

  float truthPt;
  float truthCharge;
  float truthPhi;
  float truthEta;
  float truthTheta;
  float truthD0;
  float truthZ0;
  int truthPdgId;

  int nbOfTracks;
  int nbOfFwdTracks;

  float trackPt;
  float trackCharge;
  float trackPhi;
  float trackEta;
  float trackTheta;
  float trackqOverP;
  float trackD0;
  float trackZ0;
  float trackD0PV;
  float trackZ0PV;
  float trackD0Err;
  float trackZ0Err;
  float trackRefX;
  float trackRefY;
  float trackRefZ;
  float trackqOverPErr;
  float trackD0V;
  float trackChi2;
  float trackNdof;
  float trackDeltaZSinTheta;

  float trackJetdR;
  float trackJetPt;
  int trackJetFlavour;
  int trackJetHadronConeExclTruthLabel;

  float splitTrackPt;
  float splitTrackCharge;
  float splitTrackPhi;
  float splitTrackEta;
  float splitTrackTheta;
  float splitTrackqOverP;
  float splitTrackD0;
  float splitTrackZ0;
  float splitTrackD0Err;
  float splitTrackZ0Err;
  float splitTrackqOverPErr;

  float missDistRPhi;
  float missDistZ;

  float pixeldEdx;
  int   nPixeldEdx;

  float pitchX;
  float pitchY;

  float trueD0;
  float trueZ0;
  float truePhi;
  float trueTheta;
  float trueqOverP;
  int truebarcode;
  int truepdgid;
  float truthMatchProb;

  float trackRefitPt;
  float trackRefitD0;
  float trackRefitD0Err;
  float trackRefitZ0;
  float trackRefitZ0Err;
  float trackRefitZ0SinTheta;
  float trackRefitZ0SinThetaErr;
  int trackRefitNPixelHits;
  int trackRefitNPixelLayers;

  float mindR;
  float genVtxR;
  float genVtxX;
  float genVtxY;
  float genVtxZ;
  int parentFlav;

  int hitNPixelXIBL;
  int hitNPixelYIBL;

  float hitChargeIBL;

  float hitSplitP2Cut;
  float hitSplitP3Cut;

  int hitSplitIBL;
  int hitSplitBL;
  int hitSplitL2;
  int hitSplitL3;

  float minDistIBL;
  float minDistToIBLHit;
  float minDistToL2Hit;

  float trackMatchProb;

  float hitLocalErrorXIBL;
  float hitLocalErrorYIBL;
  float trkLocalErrorXIBL;
  float trkLocalErrorYIBL;

  float hitLocalXG4IBL;
  float hitLocalYG4IBL;
  float hitLocalXIBL;
  float hitLocalYIBL;

  float phiInc;
  float etaInc;

  float pullXIBL;
  float pullYIBL;
  float resiXIBL;
  float resiYIBL;
  float pullbXIBL;
  float pullbYIBL;
  float resibXIBL;
  float resibYIBL;

  int   nPVtx;
  int   pVtxNTrk;
  float pVtxX;
  float pVtxY;
  float pVtxZ;
  float pVtxXErr;
  float pVtxYErr;
  float pVtxZErr;
  float truthPVtxX;
  float truthPVtxY;
  float truthPVtxZ;

  float iblHitVBias; 
  float blHitVBias; 

  std::vector<float> hitVBias; 
  std::vector<float> hitVDep; 
  std::vector<float> hitTemp; 
  std::vector<float> hitLorentzShift; 
  std::vector<float> hitTanLorentzAngle; 

  std::vector<int> hitLayer; 
  std::vector<float> hitCharge; 
  std::vector<int> hitToT; 
  std::vector<int> hitLVL1A; 
  std::vector<int> hitNPixel; 
  std::vector<int> hitNPixelX; 
  std::vector<int> hitNPixelY; 
  std::vector<int> hitIsSplit;
  std::vector<float> hitSplitProb1;
  std::vector<float> hitSplitProb2;
  std::vector<float> hitSplitProb3;
  std::vector<int> hitIsHole;
  std::vector<int> hitIsOutlier;
  std::vector<int> hitIs3D;
  std::vector<int> hitIsIBL;
  std::vector<int> hitIsEndCap;
  std::vector<int> hitEtaModule;  
  std::vector<int> hitPhiModule;
  std::vector<float> hitGlobalX;   
  std::vector<float> hitGlobalY;   
  std::vector<float> hitGlobalZ;   
  std::vector<float> hitLocalX; 
  std::vector<float> hitLocalY; 

  std::vector<float> clustLocalX; 
  std::vector<float> clustLocalY; 

  std::vector<float> g4LocalX; 
  std::vector<float> g4LocalY; 
  std::vector<float> g4EntryLocalX; 
  std::vector<float> g4EntryLocalY; 
  std::vector<float> g4ExitLocalX; 
  std::vector<float> g4ExitLocalY; 
  std::vector<int>   g4Barcode; 
  std::vector<int>   g4PdgId; 
  std::vector<float> g4EnergyDeposit; 
  std::vector<float> trkLocalX; 
  std::vector<float> trkLocalY; 
  std::vector<float> hitLocalErrorX; 
  std::vector<float> hitLocalErrorY; 
  std::vector<float> trkLocalErrorX; 
  std::vector<float> trkLocalErrorY; 
  std::vector<float> minTrkDistX; 
  std::vector<float> minTrkDistY; 
  std::vector<float> trkDens10; 
  std::vector<float> trkDens25; 
  std::vector<float> trkDens50; 
  std::vector<float> trkDensPitch2; 
  std::vector<float> trkDensPitch5; 
  std::vector<float> trkDensPitch10;
  std::vector<float> hitDens10; 
  std::vector<float> hitDens25; 
  std::vector<float> hitDens50;  
  std::vector<float> minTrkG4DistX; 
  std::vector<float> minTrkG4DistY; 

  std::vector<float> hit3DClusterDX; 
  std::vector<float> hit3DClusterDY; 

  std::vector<float> unbiasedResidualX; 
  std::vector<float> unbiasedResidualY; 
  std::vector<float> biasedResidualX; 
  std::vector<float> biasedResidualY;
  std::vector<float> unbiasedPullX; 
  std::vector<float> unbiasedPullY; 
  std::vector<float> biasedPullX; 
  std::vector<float> biasedPullY;

  std::vector<float> unbiasedResidualRefitX; 
  std::vector<float> unbiasedResidualRefitY; 
  std::vector<float> unbiasedResidualRefitXErr; 
  std::vector<float> unbiasedResidualRefitYErr; 
  std::vector<float> biasedResidualRefitX; 
  std::vector<float> biasedResidualRefitY; 
  std::vector<float> biasedResidualRefitXErr; 
  std::vector<float> biasedResidualRefitYErr; 
  std::vector<float> trkRefitLocalX; 
  std::vector<float> trkRefitLocalY; 

  std::vector<int> pixelLayer; 
  std::vector<int> pixelIsSplit; 
  std::vector<float> pixelLocalX; 
  std::vector<float> pixelLocalY; 
  std::vector<float> pixelLocalErrorX; 
  std::vector<float> pixelLocalErrorY; 
  std::vector<float> trkHitLocalX; 
  std::vector<float> trkHitLocalY; 
  std::vector<float> trkHitLocalErrorX; 
  std::vector<float> trkHitLocalErrorY; 

  std::vector<float> trkPhiOnSurface; 
  std::vector<float> trkThetaOnSurface; 

  std::vector< int > clus_nContributingPtcs;
  std::vector< int > clus_nContributingPU;
  std::vector< int > contributingPtcsBarcode;
  std::vector< int > contributingPtcsPdgId;
  std::vector< float > contributingPtcsPt;
  std::vector< float > contributingPtcsE;
  std::vector< std::vector< int > > clus_contributingPtcsBarcode;
  std::vector< std::vector< int > > clus_contributingPtcsPdgId;
  std::vector< std::vector< float > > clus_contributingPtcsPt;
  std::vector< std::vector< float > > clus_contributingPtcsE;

  std::vector< float > IBL_NN_phiBS;
  std::vector< float > IBL_NN_thetaBS;  
  std::vector< float > IBL_NN_matrixOfCharge;
  std::vector< float > IBL_NN_vectorOfPitchesY;
  std::vector< float > IBL_NN_localPhiPixelIndexWeightedPosition;
  std::vector< float > IBL_NN_localEtaPixelIndexWeightedPosition;

  std::vector< float > BL_NN_phiBS;
  std::vector< float > BL_NN_thetaBS;  
  std::vector< float > BL_NN_matrixOfCharge;
  std::vector< float > BL_NN_vectorOfPitchesY;
  std::vector< float > BL_NN_localPhiPixelIndexWeightedPosition;
  std::vector< float > BL_NN_localEtaPixelIndexWeightedPosition;

  std::vector< int > clus_pixelIBLIndex;
  std::vector< int > clus_pixelIBLRow;
  std::vector< int > clus_pixelIBLCol;
  std::vector< int > clus_pixelIBLCharge;
  std::vector< int > clus_pixelIBLToT;

  std::vector< int > clus_pixelBLIndex;
  std::vector< int > clus_pixelBLRow;
  std::vector< int > clus_pixelBLCol;
  std::vector< int > clus_pixelBLCharge;
  std::vector< int > clus_pixelBLToT;

  std::vector< int > clus_pixelL2Index;
  std::vector< int > clus_pixelL2Row;
  std::vector< int > clus_pixelL2Col;
  std::vector< int > clus_pixelL2Charge;
  std::vector< int > clus_pixelL2ToT;

  std::vector< int > clus_pixelL3Index;
  std::vector< int > clus_pixelL3Row;
  std::vector< int > clus_pixelL3Col;
  std::vector< int > clus_pixelL3Charge;
  std::vector< int > clus_pixelL3ToT;

  int clusterLayer;
  int clusterIsEndCap;
  int clusterToT;
  int clusterLVL1A;
  float clusterCharge;
  float clusterLocalX;
  float clusterLocalY;
  float clusterGlobalX;
  float clusterGlobalY;
  float clusterGlobalZ;
  float clusterSplitProb1;
  float clusterSplitProb2;
  int clusterIsSplit;
  int clusterNPixelsX;
  int clusterNPixelsY;
  int clusterPhiModule;
  int clusterEtaModule;
  int clusterSiHitBarcode;
  int clusterTruthBarcode;
  int clusterTruthPtcPdgId;
  float clusterTruthPtcPt;
  float clusterTrackPt;
  int holeBarrelEC; int holeLayer; int holeEtaModule; int holePhiModule;

  float iblMCRadHV;
  float ibl3dMCRadHV;
  float blMCRadHV;
  float iblP2Cut;
  float iblP3Cut;

  float minJetPt;
  float minTrackPt;
  float maxDeltaZ;
  float maxD0;
  int minPixelHits;
  int minSiHits;
  int minTRTHits;

  bool isMC;
  bool isCalibration;
  bool isCosmic;

  bool hasSiHits;
  bool hasPixelVector;
  bool hasPixelTruth;
  bool hasPixelToT;
  bool hasPixelNN;

  bool storePixelNN;

  bool doDensity;

  bool doJets;

  bool doGenTree;
  bool doTrackTree;
  bool doClusterTree;
  bool doAODTree;

  bool doGenEndCaps;
  bool doClusterEndCaps;

  bool selectPixelOverlaps;
  bool selectIBLHits;
  bool select3DHits;

  bool storeHoles;
  bool storeOutliers;

  bool hasRefit;
  bool doSplitTracks;

  int iClusterLayer;

  std::vector< int > clus_pixelIndex;
  std::vector< int > clus_pixelRow;
  std::vector< int > clus_pixelCol;
  std::vector< int > clus_pixelCharge;
  std::vector< int > clus_pixelToT;

  // this is a standard constructor
  TrackxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  float distTo3DLine(const TVector3 hit, const double *p);

  bool SolveForErrorsAndLocations(float rbx, float rby, float pbx, float pby, float rux, float ruy, float pux, float puy, float trkbx, float trkby, float trkux, float trkuy, bool firstPass, float& trkBiasedCovXY, float& trkBiasedCovXX, float& trkBiasedCovYY, float& clusCovXX, float& clusCovYY, float& trkUnbiasedCovXX, float& trkUnbiasedCovYY);

  float GetTrackBiasedCovXX(float rbx, float rby, float pbx, float pby, float trkbx, float trkux, float covTrkbxy);
  float GetTrackBiasedCovYY(float rbx, float rby, float pbx, float pby, float trkby, float trkuy, float covTrkbxy);
  std::pair<float,float> GetTrackBiasedCovXY(float rbx, float rby, float pbx, float pby, float rux, float pux, float trkbx, float trkux);

  float GetClusterCovXX(float rbx, float pbx, float covTrkbxx);
  float GetClusterCovYY(float rby, float pby, float covTrkbyy);
  float GetTrackUnbiasedCovXX(float rbx, float rux, float pbx, float pux, float covTrkbxx);
  float GetTrackUnbiasedCovYY(float rby, float ruy, float pby, float puy, float covTrkbyy);

  void GetLayerEtaPhiFromId(uint64_t id);
  //,int *barrelEC, int *layer, int *eta, int *phi);

  virtual std::vector<float> trkD0ToVtx(const xAOD::TrackParticle* track, std::vector<float> &vtxXYZ);

  xAOD::TEvent *m_event;          //!
  unsigned int  m_eventCounter;   //!
  unsigned int  m_fileCounter;   //!

 // this is needed to distribute the algorithm to the workers
  ClassDef(TrackxAODAnalysis, 1);

};

#endif
