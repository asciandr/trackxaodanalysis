#!/usr/bin/env python

import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

import ROOT
ROOT.xAOD.Init().ignore()


import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

#inputFilePath = '/eos/atlas/atlascerngroupdisk/det-ibl/DAOD/'
#ROOT.SH.ScanDir().filePattern( 'mc16Rad_13TeV.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.simul.HITS.e7142_e5984_s3126.00*.16E15.IBL1000V.DAOD_IDTRKVALID.pool.root' ).scan( sh, inputFilePath )
#ROOT.SH.ScanDir().filePattern( 'mc16Rad_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s3126.*.16E14.1000V.DAOD_IDTRKVALID.pool.root' ).scan( sh, inputFilePath )

#inputFilePath = '/eos/atlas/user/a/asciandr/TrackingCP/rel22/user.asciandr.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7.50k.R22.16E15.IBL1000V.MDN.DAOD_IDTRKVALID.e7142_e5984_s3126_EXT0/'
#ROOT.SH.ScanDir().filePattern( 'user.asciandr*' ).scan( sh, inputFilePath )
inputFilePath = '/eos/atlas/user/a/asciandr/TrackingCP/rel22/LorentzAngle/run/'
#ROOT.SH.ScanDir().filePattern( 'mc16_13TeV.361107.Zmumu.R22.NOdoRadiationDamage.NN.LA-1.DAOD_IDTRKVALID*' ).scan( sh, inputFilePath )
#ROOT.SH.ScanDir().filePattern( 'mc16_13TeV.361107.Zmumu.R22.doRadiationDamage2018.MDN.LA-1.DAOD_IDTRKVALID*' ).scan( sh, inputFilePath )
ROOT.SH.ScanDir().filePattern( 'mc16_13TeV.361107.Zmumu.R22.doRadiationDamage2018.MDN.LA0.109658.DAOD_IDTRKVALID*' ).scan( sh, inputFilePath )

#inputFilePath = '/eos/atlas/atlascerngroupdisk/det-ibl/DAOD/'
#ROOT.SH.ScanDir().filePattern( 'mc16e_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.R22.RadDamage.IBL3D_LHCbM_3_93e14_*.DAOD_IDTRKVALID.pool.root' ).scan( sh, inputFilePath ) 

#inputFilePath = '/eos/atlas/unpledged/group-tokyo/datafiles/PIXEL/IDTRKVALID/03.11.2021/data2018.v1/'
#ROOT.SH.ScanDir().filePattern( 'data18_13TeV.00357193.physics_Main.merge.DRAW_ZMUMU.f958_m1831.*.XAOD.pool.root' ).scan( sh, inputFilePath ) 

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
alg = ROOT.TrackxAODAnalysis()

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

